document.querySelector("#verifier1").addEventListener('click', function(){
	let item = document.querySelector(".questionnaire1");
	if (item.value == item.dataset["reponse"]){
		item.nextElementSibling.classList.add("check");
		item.nextElementSibling.classList.remove("cross");
		document.querySelector("#exercices").children[this.dataset["debloque"]].classList.remove("disabled");
	} else {
		item.nextElementSibling.classList.remove("check");
		item.nextElementSibling.classList.add("cross");
	}
});

document.querySelector("#verifier2").addEventListener('click', function(){
	let items = document.querySelectorAll(".questionnaire2");
	let corrects = 0;
	items.forEach((item, i) => {
		if (item.value == item.dataset["reponse"]){
			item.nextElementSibling.classList.add("check");
			item.nextElementSibling.classList.remove("cross");
			corrects++;
		} else {
			item.nextElementSibling.classList.remove("check");
			item.nextElementSibling.classList.add("cross");
		}
	});
	if (corrects == items.length){
		document.querySelector("#exercices").children[this.dataset["debloque"]].classList.remove("disabled");
	}
});

new Nim(13, ALEATOIRE, "#board1");
new Nim(13, SYMBOLIQUE, "#board2");
