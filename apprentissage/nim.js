HUMAIN = -1;
ALEATOIRE = 0;
SYMBOLIQUE = 1;
RENFORCEMENT = 2;

function percentaged(probas){
    let dico = {1: 0, 2: 0, 3: 0};
    let n = (probas.length == 0)?1:probas.length;

    for (const key of probas) {
        dico[key + 1] = dico[key + 1] + 1;
    }

    return "1 : " + Math.trunc(dico[1] * 100/n) + "%<br/>2 : " + Math.trunc(dico[2] * 100/n) + "%<br />3 : " + Math.trunc(dico[3] * 100/n) + "%";
}

class Nim{

    constructor(n, type = HUMAIN, container = "#board") {
      this.n = n;
      this.type = type;
      this.container = document.querySelector(container);
      this.last_index = n - 1;
      this.current_player = 1;
      this.show_stats = true;
      this.proba = [];
      this.proba.push([0]);
      this.proba.push([0, 1]);
      for (let i = 2; i < this.n; i++){
          this.proba.push([0, 1, 2]);
      }
      this.history = [];

      this.build();
      this.play();
    }

    check(){
        const player = this.container.children[0].children[0].children[0];
        if (this.last_index < 0){
            popup.style.display = "flex";
            document.querySelector("#popup-content").innerHTML = "<h2>Jeu de Nim</h2><p>Fin !</p><p>Le joueur gagnant est : " + player.innerHTML + "</p>";
            if (this.type == RENFORCEMENT){
                this.learn(this.current_player == 1);
            }
            return true;
        }
        return false;
    }

    build(){
        let tr_head = document.createElement('tr');
        let tr_body = document.createElement('tr');
        let tr_foot = document.createElement('tr');
        if (!this.show_stats)
            tr_foot.classList.add("hide");

        this.container.innerHTML = "<div style='justify-content: space-between;'><span>Joueur : <span class='player'>Vous</span></span><div style='justify-content: end;'><span class='button " + ((this.type!=RENFORCEMENT)?"hide":"") + "' title='Afficher les statistiques'>i</span><span class='button' title='Recommencer une partie'>Recommencer</span></div></div><table class='board'><thead></thead><tbody></tbody><tfoot></tfoot></table>";
        this.container.children[0].children[1].children[0].addEventListener('click', (event) => {
            this.show_stats = !this.show_stats;
            tr_foot.classList.toggle('hide');
        });
        this.container.children[0].children[1].children[1].addEventListener('click', (event) => {
            this.last_index = this.n - 1;
            this.current_player = 1;
            this.history = [];
            this.build();
            this.play();
        });

        for (let i = 0; i < this.n; i++){
            let th_head = document.createElement('th');
            let td_body = document.createElement('td');
            let th_foot = document.createElement('th');

            if (this.type == RENFORCEMENT){
                th_foot.innerHTML = percentaged(this.proba[i]);
            }

            td_body.innerHTML = "<img src='./assets/allumette.png' style='height: 90px; display: block; margin: auto;'/>";
            td_body.addEventListener('mouseover', event => {
                let element = event.currentTarget;
                let index = Array.from(element.parentNode.children).indexOf(element);
                if (this.current_player == 0 && !element.classList.contains("played") && index >= this.last_index - 2){
                    for (let i = index; i <= this.last_index; i++){
                        element.parentNode.children[i].classList.add("hovered");
                    }
                    if (index > 0)
                        element.parentNode.children[index - 1].classList.add("hovered-last");
                    element.parentNode.children[index].classList.add("hovered-first");
                    element.parentNode.children[this.last_index].classList.add("hovered-last");
                }

            });
            td_body.addEventListener('mouseout', event => {
                let element = event.currentTarget;
                let index = Array.from(element.parentNode.children).indexOf(element);
                if (this.current_player == 0 && !element.classList.contains("played") && index >= this.last_index - 2){
                    for (let i = index; i <= this.last_index; i++){
                        element.parentNode.children[i].classList.remove("hovered");
                    }
                    if (index > 0)
                        element.parentNode.children[index - 1].classList.remove("hovered-last");
                    element.parentNode.children[index].classList.remove("hovered-first");
                    element.parentNode.children[this.last_index].classList.remove("hovered-last");
                }
            });
            td_body.addEventListener('click', event => {
                let element = event.currentTarget;
                let index = Array.from(element.parentNode.children).indexOf(element);
                for (let i = index; i <= this.last_index; i++){
                    element.parentNode.children[i].classList.remove("hovered");
                }
                if (index > 0)
                    element.parentNode.children[index - 1].classList.remove("hovered-last");
                element.parentNode.children[index].classList.remove("hovered-first");
                element.parentNode.children[this.last_index].classList.remove("hovered-last");
                if (index >= this.last_index - 2){
                    for (let j = this.last_index; j >= index; j--){
                        element.parentNode.children[j].classList.add('played');
                        element.parentNode.children[j].style.verticalAlign = (this.current_player == 0)?"bottom":"top";
                    }
                    this.last_index = index - 1;
                    if (!this.check()){
                        this.current_player = (this.current_player + 1) % 2;
                        this.container.children[0].children[0].children[0].innerHTML = (this.current_player == 0)?"Vous":"Machine";

                        if (this.type != HUMAIN){
                            setTimeout(() => {
                                if (!this.play()){
                                    popup.style.display = "flex";
                                    document.querySelector("#popup-content").innerHTML = "<h2>Jeu de Nim</h2><p>Abandon !!!</p><p>La machine détermine qu'elle ne peut plus gagner.</p>";
                                }
                            }, 500);
                        }
                    }
                } else {
                    popup.style.display = "flex";
                    document.querySelector("#popup-content").innerHTML = "<h2>Jeu de Nim</h2><p>Coup impossible!</p><p>Vous ne pouvez prendre que 1, 2 ou 3 allumettes.</p>";
                }
            });
            tr_head.appendChild(th_head);
            tr_body.appendChild(td_body);
            tr_foot.appendChild(th_foot);
        }
        this.container.children[1].children[0].appendChild(tr_head);
        this.container.children[1].children[1].appendChild(tr_body);
        this.container.children[1].children[2].appendChild(tr_foot);
    }

    play(){
        let nb;
        switch (this.type){
            case ALEATOIRE:
                nb = Math.trunc(Math.random() * 3);
                break;
            case SYMBOLIQUE:
                nb = this.last_index % 4;
                break;
            case RENFORCEMENT:
                let probas = this.proba[this.last_index];
                if (probas.length == 0){
                    return false;
                } else {
                    let i = Math.trunc(Math.random() * probas.length);
                    nb = probas[i];
                    this.history.push([this.last_index, i]);
                }
                break;
        }
        let index = Math.max(0, this.last_index - nb);
        let tr = this.container.children[1].children[1].children[0];

        for (let j = this.last_index; j >= index; j--){
            tr.children[j].classList.add('played');
            tr.children[j].style.verticalAlign = "top";

            this.last_index--;
        }
        if (!this.check()){
            this.current_player = (this.current_player + 1) % 2;
            this.container.children[0].children[0].children[0].innerHTML = (this.current_player == 0)?"Vous":"Machine";
        }
        return true;
    }

    learn(is_winner){
        if (!is_winner && this.history.length > 0){
            let index = this.history[this.history.length - 1][0];
            let i = this.history[this.history.length - 1][1];
            this.proba[index] = this.proba[index].slice(0, i).concat(this.proba[index].slice(i + 1));
        } else if (is_winner){
            for (let x = 0; x < this.history.length; x++){
                let index = this.history[x][0];
                let i = this.history[x][1];
                this.proba[index].push(this.proba[index][i]);
            }
        }
    }
}
