document.querySelectorAll(".info").forEach((item, i) => {
	item.addEventListener('click', function(){
		document.querySelector("#popup-content").innerHTML = this.dataset["info"];
	    popup.style.display = "flex";
	});
});

document.querySelector("#verifier1").addEventListener('click', function(){
	let items = document.querySelectorAll(".questionnaire1");
	let corrects = 0;
	items.forEach((item, i) => {
		if (item.value == item.dataset["reponse"]){
			item.nextElementSibling.classList.add("check");
			item.nextElementSibling.classList.remove("cross");
			corrects++;
		} else {
			item.nextElementSibling.classList.remove("check");
			item.nextElementSibling.classList.add("cross");
		}
	});
	if (corrects == items.length){
		document.querySelector("#exercices").children[this.dataset["debloque"]].classList.remove("disabled");
	}
});
