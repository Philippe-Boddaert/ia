const popup = document.querySelector("#popup");

const histoire = "<div class='block'><h2>Description</h2><p>L'objectif est de construire un classifieur de smileys.</p><p>Étant donné un smiley en entrée, le classifieur indique si ce smiley représente un visage heureux ou triste.</p><p>Le classifieur nécessite que l'on attribue des pondérations aux pixels d’entrée de l’image en cliquant dessus.</p><p>Un clic définit la pondération sur 1 et un deuxième clic la définit sur -1.</p><p>L’activation positive indique que l’image est classée comme un visage heureux, ce qui peut être correct ou non, tandis que l’activation négative indique que l’image est classée comme visage triste.</p></div>"

function preventDefaults (e) {
	e.preventDefault();
	e.stopPropagation();
}

document.querySelector("#close").addEventListener('click', function(){
    popup.style.display = "none";
});

document.querySelector("#history").addEventListener('click', function(){
    document.querySelector("#popup-content").innerHTML = histoire;
    popup.style.display = "flex";
});
