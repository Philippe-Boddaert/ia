const popup = document.querySelector("#popup");

const histoire = "<div class='block'><h2>Description</h2><p>L'objectif de cette section est de comprendre le fonctionnement du réseau de neurones.</p><p style='text-align: center;'><img src='assets/cerveau.png' /></p><p>Composant essentiel du cerveau humain, le réseau neuronal intervient dans les fonctions d'acquisition, de mémorisation de l'information et d'apprentissage au fil du temps.</p></div>"

function preventDefaults (e) {
	e.preventDefault();
	e.stopPropagation();
}

document.querySelector("#close").addEventListener('click', function(){
    popup.style.display = "none";
});

document.querySelector("#history").addEventListener('click', function(){
    document.querySelector("#popup-content").innerHTML = histoire;
    popup.style.display = "flex";
});

document.querySelectorAll(".pop").forEach((item, i) => {
	item.addEventListener('click', function(){
		if (this.dataset["href"])
			location.href = this.dataset["href"];
	});
});
