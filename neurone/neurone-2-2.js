// ==============================
//       CANVAS 1
// ==============================

let _wA2 = 0;
let _wB2 = 0;
let _seuil2 = 0;

const canvas2 = document.getElementById("reseau2");
const stage2 = new createjs.Stage(canvas2);
stage2.enableMouseOver();
createjs.Touch.enable(stage2);
stage2.mouseMoveOutside = true;

const container2 = new createjs.Container();
stage2.addChild(container2);
container2.visible = false;

const dentriteA2 = new createjs.Shape();
stage2.addChild(dentriteA2);
dentriteA2.x = 70;
dentriteA2.y = 100;
dentriteA2.graphics.ss(8, "round").s('#31C2A9').mt(0, 0).bt(40, -20, 40, 95, 80, 75);

const dentriteB2 = new createjs.Shape();
stage2.addChild(dentriteB2);
dentriteB2.x = 70;
dentriteB2.y = 300;
dentriteB2.graphics.ss(8, "round").s('#31C2A9').mt(0, 0).bt(40, 20, 40, -95, 80, -75);

const axone2 = new createjs.Shape();
stage2.addChild(axone2);
axone2.x = 250;
axone2.y = 200;
axone2.graphics.ss(12, "round").s('#31C2A9').mt(0, 0).bt(30, 10, 60, -10, 80, 0);

const entreeA2 = new createjs.Shape();
stage2.addChild(entreeA2);
entreeA2.x = 50;
entreeA2.y = 100;
entreeA2.cursor = "pointer";
entreeA2.graphics.s("#000").ss(1).f("#FFF").dc(0, 0, 20);

const textA2 = new createjs.Text("A", "bold 20px Verdana", "#000");
stage2.addChild(textA2);
textA2.x = entreeA2.x;
textA2.y = entreeA2.y - 40;
textA2.textAlign = 'center';

const valeurA2 = new createjs.Text("0", "bold 20px Verdana", "#000");
stage2.addChild(valeurA2);
valeurA2.x = entreeA2.x;
valeurA2.y = entreeA2.y - 10;
valeurA2.textAlign = 'center';

entreeA2.addEventListener('click', function(){
	valeurA2.text = (valeurA2.text == "0")?"1":"0";
	calculerS2();
	stage2.update();
});

const entreeB2 = new createjs.Shape();
stage2.addChild(entreeB2);
entreeB2.x = 50;
entreeB2.y = 300;
entreeB2.cursor = "pointer";
entreeB2.graphics.s("#000").ss(1).f("#FFF").dc(0, 0, 20);

const valeurB2 = new createjs.Text("0", "bold 20px Verdana", "#000");
stage2.addChild(valeurB2);
valeurB2.x = entreeB2.x;
valeurB2.y = entreeB2.y - 10;
valeurB2.textAlign = 'center';

entreeB2.addEventListener('click', function(){
	valeurB2.text = (valeurB2.text == "0")?"1":"0";
	calculerS2();
	stage2.update();
});

const textB2 = new createjs.Text("B", "bold 20px Verdana", "#000");
stage2.addChild(textB2);
textB2.x = entreeB2.x;
textB2.y = entreeB2.y + 25;
textB2.textAlign = 'center';

const sortie2 = new createjs.Shape();
stage2.addChild(sortie2);
sortie2.x = 350;
sortie2.y = 200;
sortie2.graphics.s("#000").ss(1).f("#FFF").dc(0, 0, 20);

const textS2 = new createjs.Text("S", "bold 20px Verdana", "#000");
stage2.addChild(textS2);
textS2.x = sortie2.x;
textS2.y = sortie2.y - 40;
textS2.textAlign = 'center';

const valeurS2 = new createjs.Text("-", "bold 20px Verdana", "#000");
stage2.addChild(valeurS2);
valeurS2.x = sortie2.x;
valeurS2.y = sortie2.y - 10;
valeurS2.textAlign = 'center';

const corps2 = new createjs.Shape();
stage2.addChild(corps2);
corps2.x = 200 - 50;
corps2.y = 200 - 37;
corps2.graphics.s("#000").ss(1).f("#FFF").r(0, 0, 100, 75);

const seuilS2 = new createjs.Text("Seuil : 0", "bold 12px Verdana", "#000");
stage2.addChild(seuilS2);
seuilS2.x = corps1.x + 60;
seuilS2.y = corps1.y - 15;
seuilS2.textAlign = 'left';

const wA2 = new createjs.Text("w  : 0", "bold 12px Verdana", "#000");
stage2.addChild(wA2);
wA2.x = dentriteA2.x + 40;
wA2.y = dentriteA2.y + 5;
wA2.textAlign = 'left';

const w_A2 = new createjs.Text("A", "bold 12px Verdana", "#000");
stage2.addChild(w_A2);
w_A2.x = wA2.x + 10;
w_A2.y = wA2.y + 5;
w_A2.textAlign = 'left';

const xBarresA2 = wA2.x;
const yBarresA2 = wA2.y - 15;

const fondBarresA2 = new createjs.Shape();
container2.addChild(fondBarresA2);
fondBarresA2.graphics.s(1, 1, 1).ss('#000').f('#666').rr(xBarresA2, yBarresA2, 54, 8, 3);
fondBarresA2.graphics.s(3).ss('#444').mt(xBarresA2 + 4, yBarresA2 + 4).lt(xBarresA2 + 4 + 50, yBarresA2 + 4);

const curseurBarresA2 = new createjs.Shape();
container2.addChild(curseurBarresA2);
curseurBarresA2.cursor = 'pointer'
curseurBarresA2.graphics.s(1).ss('#000').f('#00F').dc(0, 0, 6).ef().es();
curseurBarresA2.x = xBarresA2 + ((_wA2 + 1) * 25);
curseurBarresA2.y = yBarresA2 + 4;

const levelBarresA2 = new createjs.Shape();
container2.addChild(levelBarresA2);

for (let niveau = 0; niveau <= 50; niveau += 25){
    const text = new createjs.Text((niveau - 25) / 25, "8px Verdana", "#000");
    text.x = xBarresA2 + niveau - 2;
    text.y = yBarresA2 - 20;
    text.cursor = 'pointer';
    text.textAlign = 'left';
    container2.addChild(text);

	levelBarresA2.graphics.s('#444').ss(1).mt(text.x + 3, text.y + 10).lt(text.x + 3, text.y + 15);

    text.addEventListener("click", function(evt){
		obj = evt.currentTarget;
		_wA2 = (obj.x - xBarresA2 + 2 - 25) / 25;
		curseurBarresA2.x = obj.x + 2;
		wA2.text = "w  : " + _wA2;
		calculerS2();
	    stage2.update();
	});
}

const wB2 = new createjs.Text("w  : 0", "bold 12px Verdana", "#000");
stage2.addChild(wB2);
wB2.x = dentriteB2.x + 40;
wB2.y = dentriteB2.y - 15;
wB2.textAlign = 'left';

const w_B2 = new createjs.Text("B", "bold 12px Verdana", "#000");
stage2.addChild(w_B2);
w_B2.x = wB2.x + 10;
w_B2.y = wB2.y + 5;
w_B2.textAlign = 'left';

const combinaison2 = new createjs.Text("Combinaison : 0", "bold 10px Verdana", "#000");
stage2.addChild(combinaison2);
combinaison2.x = corps2.x + 5;
combinaison2.y = corps2.y + 25;
combinaison2.textAlign = 'left';

const formule2 = new createjs.Text("A * wA + B * wB", "bold 10px Verdana", "#000");
stage2.addChild(formule2);
formule2.x = corps2.x + 3;
formule2.y = corps2.y + 45;
formule2.textAlign = 'left';

const xBarresB2 = wB2.x;
const yBarresB2 = wB2.y + 25;

const fondBarresB2 = new createjs.Shape();
container2.addChild(fondBarresB2);
fondBarresB2.graphics.s(1, 1, 1).ss('#000').f('#666').rr(xBarresB2, yBarresB2, 54, 8, 3);
fondBarresB2.graphics.s(3).ss('#444').mt(xBarresB2 + 4, yBarresB2 + 4).lt(xBarresB2 + 4 + 50, yBarresB2 + 4);

const curseurBarresB2 = new createjs.Shape();
container2.addChild(curseurBarresB2);
curseurBarresB2.cursor = 'pointer'
curseurBarresB2.graphics.s(1).ss('#000').f('#00F').dc(0, 0, 6).ef().es();
curseurBarresB2.x = xBarresB2 + ((_wB2 + 1) * 25);
curseurBarresB2.y = yBarresB2 + 4;

const levelBarresB2 = new createjs.Shape();
container2.addChild(levelBarresB2);

for (let niveau = 0; niveau <= 50; niveau += 25){
    const text = new createjs.Text((niveau - 25) / 25, "8px Verdana", "#000");
    text.x = xBarresB2 + niveau - 2;
    text.y = yBarresB2 + 20;
    text.cursor = 'pointer';
    text.textAlign = 'left';
    container2.addChild(text);

	levelBarresB2.graphics.s('#444').ss(1).mt(text.x + 3, text.y - 2).lt(text.x + 3, text.y - 7);

    text.addEventListener("click", function(evt){
		obj = evt.currentTarget;
		_wB2 = (obj.x - xBarresB2 + 2 - 25) / 25;
		curseurBarresB2.x = obj.x + 2;
		wB2.text = "w  : " + _wB2;
		calculerS2();
	    stage2.update();
	});
}

const xSeuil2 = seuilS2.x;
const ySeuil2 = seuilS2.y - 15;

const fondBarresS2 = new createjs.Shape();
container2.addChild(fondBarresS2);
fondBarresS2.graphics.s(1, 1, 1).ss('#000').f('#666').rr(xSeuil2, ySeuil2, 54, 8, 3);
fondBarresS2.graphics.s(3).ss('#444').mt(xSeuil2 + 4, ySeuil2 + 4).lt(xSeuil2 + 4 + 50, ySeuil2 + 4);

const curseurBarresS2 = new createjs.Shape();
container2.addChild(curseurBarresS2);
curseurBarresS2.cursor = 'pointer'
curseurBarresS2.graphics.s(1).ss('#000').f('#F00').dc(0, 0, 6).ef().es();
curseurBarresS2.x = xSeuil2 + ((_seuil2 + 1) * 25);
curseurBarresS2.y = ySeuil2 + 4;

const levelBarresS2 = new createjs.Shape();
container2.addChild(levelBarresS2);

for (let niveau = 0; niveau <= 50; niveau += 25){
    const text = new createjs.Text((niveau - 25) / 12.5, "8px Verdana", "#000");
    text.x = xSeuil2 + niveau - 2;
    text.y = ySeuil2 - 20;
    text.cursor = 'pointer';
    text.textAlign = 'left';
    container2.addChild(text);

	levelBarresS2.graphics.s('#444').ss(1).mt(text.x + 3, text.y + 10).lt(text.x + 3, text.y + 15);

    text.addEventListener("click", function(evt){
		obj = evt.currentTarget;
		_seuil2 = (((obj.x - xSeuil2) + 2 - 25) / 12.5);
		curseurBarresS2.x = obj.x + 2;
		seuilS2.text = "Seuil : " + _seuil2;
		calculerS2();
	    stage2.update();
	});
}

curseurBarresA2.addEventListener("mousedown", onMouseDownCurseur);
curseurBarresB2.addEventListener("mousedown", onMouseDownCurseur);
curseurBarresS2.addEventListener("mousedown", onMouseDownCurseur);

curseurBarresA2.addEventListener("pressmove", function(evt){
	obj = evt.currentTarget;
	obj.x = Math.min(Math.max(xBarresA2, evt.stageX + obj.offsetX), xBarresA2 + 50);
	_wA2 = (((obj.x - xBarresA2) - 25) / 25);
	wA2.text = "w  : " + _wA2;
	calculerS2();
	stage2.update();
});

curseurBarresB2.addEventListener("pressmove", function(evt){
	obj = evt.currentTarget;
	obj.x = Math.min(Math.max(xBarresB2, evt.stageX + obj.offsetX), xBarresB2 + 50);
	_wB2 = (((obj.x - xBarresB2) - 25) / 25);
	wB2.text = "w  : " + _wB2;
	calculerS2();
	stage2.update();
});

curseurBarresS2.addEventListener("pressmove", function(evt){
	obj = evt.currentTarget;
	obj.x = Math.min(Math.max(xSeuil2, evt.stageX + obj.offsetX), xSeuil2 + 50);
	_seuil2 = (((obj.x - xSeuil2) - 25) / 12.5);
	seuilS2.text = "Seuil : " + _seuil2;
	calculerS2();
	stage2.update();
});

const info2 = new createjs.Shape();
stage2.addChild(info2);
info2.x = 320;
info2.y = 350;
info2.graphics.s('#5163ad').ss(5).f("#1a3e70").rr(0, 0, 30, 40, 5, 5, 5, 5);
info2.cursor = 'pointer';

const textInfo2 = new createjs.Text("i", "bold 18px Verdana", "#fba501");
stage2.addChild(textInfo2);
textInfo2.x = info2.x + 12;
textInfo2.y = info2.y + 10;

const barreInfo2 = new createjs.Shape();
stage2.addChild(barreInfo2);
barreInfo2.x = 331;
barreInfo2.y = 375;
barreInfo2.graphics.s('#fba501').ss(3).mt(0, 0).lt(8, -8);

info2.addEventListener('mouseover', function(){
	info2.graphics.clear();
	info2.graphics.s('#fba501').ss(5).f("#5163ad").rr(0, 0, 30, 40, 5, 5, 5, 5);
	info2.cursor = 'pointer';
	barreInfo2.graphics.clear();
	barreInfo2.graphics.s('#1a3e70').ss(3).mt(0, 0).lt(8, -8);
	textInfo2.color = '#1a3e70';
	stage2.update();
});

info2.addEventListener('mouseout', function(){
	info2.graphics.clear();
	info2.graphics.s('#5163ad').ss(5).f("#1a3e70").rr(0, 0, 30, 40, 5, 5, 5, 5);
	info2.cursor = 'pointer';
	barreInfo2.graphics.clear();
	barreInfo2.graphics.s('#fba501').ss(3).mt(0, 0).lt(8, -8);
	textInfo2.color = '#fba501';
	stage2.update();
});
const containerLegende2 = new createjs.Container();
stage2.addChild(containerLegende2);

const textEntree2 = new createjs.Text("Entrée", "bold 18px Verdana", "#000");
containerLegende2.addChild(textEntree2);
textEntree2.x = 10;
textEntree2.y = 10;

const ligneEntree2 = new createjs.Shape();
containerLegende2.addChild(ligneEntree2);
ligneEntree2.graphics.s('#444').ss(3).sd([10, 10]).mt(90, 0).lt(90, 400);

const ligneSortie2 = new createjs.Shape();
containerLegende2.addChild(ligneSortie2);
ligneSortie2.graphics.s('#444').ss(3).sd([10, 10]).mt(310, 0).lt(310, 400);

const textNeurone2 = new createjs.Text("Neurone", "bold 18px Verdana", "#000");
containerLegende2.addChild(textNeurone2);
textNeurone2.x = 160;
textNeurone2.y = 10;

const textSortie2 = new createjs.Text("Sortie", "bold 18px Verdana", "#000");
containerLegende2.addChild(textSortie2);
textSortie2.x = 330;
textSortie2.y = 10;

info2.addEventListener('click', function(){
	containerLegende2.visible = !containerLegende2.visible;
	barreInfo2.visible = containerLegende2.visible;
	stage2.update();
});

const show2 = new createjs.Shape();
stage2.addChild(show2);
show2.x = 360;
show2.y = 350;
show2.graphics.s('#5163ad').ss(5).f("#1a3e70").rr(0, 0, 30, 40, 5, 5, 5, 5);
show2.cursor = 'pointer';

show2.addEventListener('mouseover', function(){
	show2.graphics.clear();
	show2.graphics.s('#fba501').ss(5).f("#5163ad").rr(0, 0, 30, 40, 5, 5, 5, 5);
	show2.cursor = 'pointer';
	textShow2.color = '#1a3e70';
	stage2.update();
});

show2.addEventListener('mouseout', function(){
	show2.graphics.clear();
	show2.graphics.s('#5163ad').ss(5).f("#1a3e70").rr(0, 0, 30, 40, 5, 5, 5, 5);
	show2.cursor = 'pointer';
	textShow2.color = '#fba501';
	stage2.update();
});

show2.addEventListener('click', function(){
	container2.visible = !container2.visible;
	textShow2.text = (container2.visible)?"≠":"=";
	stage2.update();
});

const textShow2 = new createjs.Text("=", "bold 18px Verdana", "#fba501");
stage2.addChild(textShow2);
textShow2.x = show2.x + 7;
textShow2.y = show2.y + 10;

function calculerS2(){
	let combinaison = parseInt(valeurA2.text) * _wA2 + parseInt(valeurB2.text) * _wB2;
	combinaison2.text = "Combinaison : " + combinaison;
	if (combinaison > _seuil2){
		valeurS2.text = 1;
	} else {
		valeurS2.text = 0;
	}
}

verifier2.addEventListener('click', function(){
	const lignes = document.querySelectorAll("#ou tbody tr");
	let corrects = 0;

	lignes.forEach((ligne, i) => {
		let c = (ligne.children[0].innerHTML * _wA2) + (ligne.children[1].innerHTML * _wB2);
		let s = (c > _seuil2)?1:0;
		if (s == ligne.children[2].innerHTML){
			ligne.classList.add("ok");
			ligne.classList.remove("nok");
			corrects++;
		} else {
			ligne.classList.add("nok");
			ligne.classList.remove("ok");
		}
	});

	if (corrects == 4){
		document.querySelector("#exercices").children[this.dataset["debloque"]].classList.remove("disabled");
	}
});

stage2.update();
