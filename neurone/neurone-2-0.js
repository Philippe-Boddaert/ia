// ==============================
//       CANVAS 1
// ==============================

let _wA0 = 0;
let _seuil0 = 0;

const canvas0 = document.getElementById("reseau0");
const stage0 = new createjs.Stage(canvas0);
stage0.enableMouseOver();
createjs.Touch.enable(stage0);
stage0.mouseMoveOutside = true;

const container0 = new createjs.Container();
stage0.addChild(container0);
container0.visible = false;

const dentriteA0 = new createjs.Shape();
stage0.addChild(dentriteA0);
dentriteA0.x = 70;
dentriteA0.y = 200;
dentriteA0.graphics.ss(8, "round").s('#31C2A9').mt(0, 0).bt(30, 10, 60, -10, 80, 0);

const axone0 = new createjs.Shape();
stage0.addChild(axone0);
axone0.x = 250;
axone0.y = 200;
axone0.graphics.ss(12, "round").s('#31C2A9').mt(0, 0).bt(30, 10, 60, -10, 80, 0);

const entreeA0 = new createjs.Shape();
stage0.addChild(entreeA0);
entreeA0.x = 50;
entreeA0.y = 200;
entreeA0.cursor = "pointer";
entreeA0.graphics.s("#000").ss(1).f("#FFF").dc(0, 0, 20);

const textA0 = new createjs.Text("A", "bold 20px Verdana", "#000");
stage0.addChild(textA0);
textA0.x = entreeA0.x;
textA0.y = entreeA0.y - 40;
textA0.textAlign = 'center';

const valeurA0 = new createjs.Text("0", "bold 20px Verdana", "#000");
stage0.addChild(valeurA0);
valeurA0.x = entreeA0.x;
valeurA0.y = entreeA0.y - 10;
valeurA0.textAlign = 'center';

entreeA0.addEventListener('click', function(){
	valeurA0.text = (valeurA0.text == "0")?"1":"0";
	calculerS0();
	stage0.update();
});

const sortie0 = new createjs.Shape();
stage0.addChild(sortie0);
sortie0.x = 350;
sortie0.y = 200;
sortie0.graphics.s("#000").ss(1).f("#FFF").dc(0, 0, 20);

const textS0 = new createjs.Text("S", "bold 20px Verdana", "#000");
stage0.addChild(textS0);
textS0.x = sortie0.x;
textS0.y = sortie0.y - 40;
textS0.textAlign = 'center';

const valeurS0 = new createjs.Text("-", "bold 20px Verdana", "#000");
stage0.addChild(valeurS0);
valeurS0.x = sortie0.x;
valeurS0.y = sortie0.y - 10;
valeurS0.textAlign = 'center';

const corpS0 = new createjs.Shape();
stage0.addChild(corpS0);
corpS0.x = 200 - 50;
corpS0.y = 200 - 37;
corpS0.graphics.s("#000").ss(1).f("#FFF").r(0, 0, 100, 75);

const seuilS0 = new createjs.Text("Seuil : 0", "bold 12px Verdana", "#000");
stage0.addChild(seuilS0);
seuilS0.x = corpS0.x + 60;
seuilS0.y = corpS0.y - 15;
seuilS0.textAlign = 'left';

const wA0 = new createjs.Text("w  : 0", "bold 12px Verdana", "#000");
stage0.addChild(wA0);
wA0.x = dentriteA0.x + 10;
wA0.y = dentriteA0.y - 20;
wA0.textAlign = 'left';

const w_A0 = new createjs.Text("A", "bold 12px Verdana", "#000");
stage0.addChild(w_A0);
w_A0.x = wA0.x + 10;
w_A0.y = wA0.y + 5;
w_A0.textAlign = 'left';

const xBarresA0 = wA0.x;
const yBarresA0 = wA0.y - 15;

const fondBarresA0 = new createjs.Shape();
container0.addChild(fondBarresA0);
fondBarresA0.graphics.s(1, 1, 1).ss('#000').f('#666').rr(xBarresA0, yBarresA0, 54, 8, 3);
fondBarresA0.graphics.s(3).ss('#444').mt(xBarresA0 + 4, yBarresA0 + 4).lt(xBarresA0 + 4 + 50, yBarresA0 + 4);

const curseurBarresA0 = new createjs.Shape();
container0.addChild(curseurBarresA0);
curseurBarresA0.cursor = 'pointer'
curseurBarresA0.graphics.s(1).ss('#000').f('#00F').dc(0, 0, 6).ef().es();
curseurBarresA0.x = xBarresA0 + ((_wA0 + 1) * 25);
curseurBarresA0.y = yBarresA0 + 4;

const levelBarresA0 = new createjs.Shape();
container0.addChild(levelBarresA0);

for (let niveau = 0; niveau <= 50; niveau += 25){
    const text = new createjs.Text((niveau - 25) / 25, "8px Verdana", "#000");
    text.x = xBarresA0 + niveau - 2;
    text.y = yBarresA0 - 20;
    text.cursor = 'pointer';
    text.textAlign = 'left';
    container0.addChild(text);

	levelBarresA0.graphics.s('#444').ss(1).mt(text.x + 3, text.y + 10).lt(text.x + 3, text.y + 15);

    text.addEventListener("click", function(evt){
		obj = evt.currentTarget;
		_wA0 = (obj.x - xBarresA0 + 2 - 25) / 25;
		curseurBarresA0.x = obj.x + 2;
		wA0.text = "w  : " + _wA0;
		calculerS0();
	    stage0.update();
	});
}


const combinaison0 = new createjs.Text("Combinaison : 0", "bold 10px Verdana", "#000");
stage0.addChild(combinaison0);
combinaison0.x = corpS0.x + 5;
combinaison0.y = corpS0.y + 25;
combinaison0.textAlign = 'left';

const formule0 = new createjs.Text("A * wA", "bold 10px Verdana", "#000");
stage0.addChild(formule0);
formule0.x = corpS0.x + 28;
formule0.y = corpS0.y + 45;
formule0.textAlign = 'left';

const xSeuil0 = seuilS0.x;
const ySeuil0 = seuilS0.y - 15;

const fondBarresS0 = new createjs.Shape();
container0.addChild(fondBarresS0);
fondBarresS0.graphics.s(1, 1, 1).ss('#000').f('#666').rr(xSeuil0, ySeuil0, 54, 8, 3);
fondBarresS0.graphics.s(3).ss('#444').mt(xSeuil0 + 4, ySeuil0 + 4).lt(xSeuil0 + 4 + 50, ySeuil0 + 4);

const curseurBarresS0 = new createjs.Shape();
container0.addChild(curseurBarresS0);
curseurBarresS0.cursor = 'pointer'
curseurBarresS0.graphics.s(1).ss('#000').f('#F00').dc(0, 0, 6).ef().es();
curseurBarresS0.x = xSeuil0 + ((_seuil0 + 1) * 25);
curseurBarresS0.y = ySeuil0 + 4;

const levelBarresS0 = new createjs.Shape();
container0.addChild(levelBarresS0);

for (let niveau = 0; niveau <= 50; niveau += 25){
    const text = new createjs.Text((niveau - 25) / 12.5, "8px Verdana", "#000");
    text.x = xSeuil0 + niveau - 2;
    text.y = ySeuil0 - 20;
    text.cursor = 'pointer';
    text.textAlign = 'left';
    container0.addChild(text);

	levelBarresS0.graphics.s('#444').ss(1).mt(text.x + 3, text.y + 10).lt(text.x + 3, text.y + 15);

    text.addEventListener("click", function(evt){
		obj = evt.currentTarget;
		_seuil0 = (((obj.x - xSeuil0) + 2 - 25) / 12.5);
		curseurBarresS0.x = obj.x + 2;
		seuilS0.text = "Seuil : " + _seuil0;
		calculerS0();
	    stage0.update();
	});
}

function onMouseDownCurseur(evt) {
    obj = evt.currentTarget;
    obj.offsetX = obj.x - evt.stageX;
}

curseurBarresA0.addEventListener("mousedown", onMouseDownCurseur);
curseurBarresS0.addEventListener("mousedown", onMouseDownCurseur);

curseurBarresA0.addEventListener("pressmove", function(evt){
	obj = evt.currentTarget;
	obj.x = Math.min(Math.max(xBarresA0, evt.stageX + obj.offsetX), xBarresA0 + 50);
	_wA0 = (((obj.x - xBarresA0) - 25) / 25);
	wA0.text = "w  : " + _wA0;
	calculerS0();
	stage0.update();
});

curseurBarresS0.addEventListener("pressmove", function(evt){
	obj = evt.currentTarget;
	obj.x = Math.min(Math.max(xSeuil0, evt.stageX + obj.offsetX), xSeuil0 + 50);
	_seuil0 = (((obj.x - xSeuil0) - 25) / 12.5);
	seuilS0.text = "Seuil : " + _seuil0;
	calculerS0();
	stage0.update();
});

const info0 = new createjs.Shape();
stage0.addChild(info0);
info0.x = 320;
info0.y = 350;
info0.graphics.s('#5163ad').ss(5).f("#1a3e70").rr(0, 0, 30, 40, 5, 5, 5, 5);
info0.cursor = 'pointer';

const textInfo0 = new createjs.Text("i", "bold 18px Verdana", "#fba501");
stage0.addChild(textInfo0);
textInfo0.x = info0.x + 12;
textInfo0.y = info0.y + 10;

const barreInfo0 = new createjs.Shape();
stage0.addChild(barreInfo0);
barreInfo0.x = 331;
barreInfo0.y = 375;
barreInfo0.graphics.s('#fba501').ss(3).mt(0, 0).lt(8, -8);

info0.addEventListener('mouseover', function(){
	info0.graphics.clear();
	info0.graphics.s('#fba501').ss(5).f("#5163ad").rr(0, 0, 30, 40, 5, 5, 5, 5);
	info0.cursor = 'pointer';
	barreInfo0.graphics.clear();
	barreInfo0.graphics.s('#1a3e70').ss(3).mt(0, 0).lt(8, -8);
	textInfo0.color = '#1a3e70';
	stage0.update();
});

info0.addEventListener('mouseout', function(){
	info0.graphics.clear();
	info0.graphics.s('#5163ad').ss(5).f("#1a3e70").rr(0, 0, 30, 40, 5, 5, 5, 5);
	info0.cursor = 'pointer';
	barreInfo0.graphics.clear();
	barreInfo0.graphics.s('#fba501').ss(3).mt(0, 0).lt(8, -8);
	textInfo0.color = '#fba501';
	stage0.update();
});
const containerLegende0 = new createjs.Container();
stage0.addChild(containerLegende0);

const textEntree0 = new createjs.Text("Entrée", "bold 18px Verdana", "#000");
containerLegende0.addChild(textEntree0);
textEntree0.x = 10;
textEntree0.y = 10;

const ligneEntree0 = new createjs.Shape();
containerLegende0.addChild(ligneEntree0);
ligneEntree0.graphics.s('#444').ss(3).sd([10, 10]).mt(90, 0).lt(90, 400);

const ligneSortie0 = new createjs.Shape();
containerLegende0.addChild(ligneSortie0);
ligneSortie0.graphics.s('#444').ss(3).sd([10, 10]).mt(310, 0).lt(310, 400);

const textNeurone0 = new createjs.Text("Neurone", "bold 18px Verdana", "#000");
containerLegende0.addChild(textNeurone0);
textNeurone0.x = 160;
textNeurone0.y = 10;

const textSortie0 = new createjs.Text("Sortie", "bold 18px Verdana", "#000");
containerLegende0.addChild(textSortie0);
textSortie0.x = 330;
textSortie0.y = 10;

info0.addEventListener('click', function(){
	containerLegende0.visible = !containerLegende0.visible;
	barreInfo0.visible = containerLegende0.visible;
	stage0.update();
});

const show0 = new createjs.Shape();
stage0.addChild(show0);
show0.x = 360;
show0.y = 350;
show0.graphics.s('#5163ad').ss(5).f("#1a3e70").rr(0, 0, 30, 40, 5, 5, 5, 5);
show0.cursor = 'pointer';

show0.addEventListener('mouseover', function(){
	show0.graphics.clear();
	show0.graphics.s('#fba501').ss(5).f("#5163ad").rr(0, 0, 30, 40, 5, 5, 5, 5);
	show0.cursor = 'pointer';
	textShow0.color = '#1a3e70';
	stage0.update();
});

show0.addEventListener('mouseout', function(){
	show0.graphics.clear();
	show0.graphics.s('#5163ad').ss(5).f("#1a3e70").rr(0, 0, 30, 40, 5, 5, 5, 5);
	show0.cursor = 'pointer';
	textShow0.color = '#fba501';
	stage0.update();
});

show0.addEventListener('click', function(){
	container0.visible = !container0.visible;
	textShow0.text = (container0.visible)?"≠":"=";
	stage0.update();
});

const textShow0 = new createjs.Text("=", "bold 18px Verdana", "#fba501");
stage0.addChild(textShow0);
textShow0.x = show0.x + 7;
textShow0.y = show0.y + 10;

function calculerS0(){
	let combinaison = parseInt(valeurA0.text) * _wA0;
	combinaison0.text = "Combinaison : " + combinaison;
	if (combinaison > _seuil0){
		valeurS0.text = 1;
	} else {
		valeurS0.text = 0;
	}
}

verifier0.addEventListener('click', function(){
	const lignes = document.querySelectorAll("#identite tbody tr");
	let corrects = 0;

	lignes.forEach((ligne, i) => {
		let c = ligne.children[0].innerHTML * _wA0;
		let s = (c > _seuil0)?1:0;
		if (s == ligne.children[1].innerHTML){
			ligne.classList.add("ok");
			ligne.classList.remove("nok");
			corrects++;
		} else {
			ligne.classList.add("nok");
			ligne.classList.remove("ok");
		}
	});

	if (corrects == 2){
		document.querySelector("#exercices").children[this.dataset["debloque"]].classList.remove("disabled");
	}
});

stage0.update();
