// ==============================
//       CANVAS 1
// ==============================

const portes = {
	"": (a, b) => 0,
	"ID" : (a) => a,
	"NON" : (a) => (a == 0)?1:0,
	"ET" : (a, b) => a && b,
	"OU" : (a, b) => a || b
}

let _wA4 = 0;
let _seuil4 = 0;

const canvas4 = document.getElementById("reseau4");
const stage4 = new createjs.Stage(canvas4);
stage4.enableMouseOver();
createjs.Touch.enable(stage4);
stage4.mouseMoveOutside = true;

const container4 = new createjs.Container();
stage4.addChild(container4);
container4.visible = false;

const entreeA4 = new createjs.Shape();
stage4.addChild(entreeA4);
entreeA4.x = 25;
entreeA4.y = 150;
entreeA4.cursor = "pointer";
entreeA4.graphics.s("#000").ss(1).f("#FFF").dc(0, 0, 20);

const textA4 = new createjs.Text("A", "bold 20px Verdana", "#000");
stage4.addChild(textA4);
textA4.x = entreeA4.x;
textA4.y = entreeA4.y - 40;
textA4.textAlign = 'center';

const valeurA4 = new createjs.Text("0", "bold 20px Verdana", "#000");
stage4.addChild(valeurA4);
valeurA4.x = entreeA4.x;
valeurA4.y = entreeA4.y - 10;
valeurA4.textAlign = 'center';

entreeA4.addEventListener('click', function(){
	valeurA4.text = (valeurA4.text == "0")?"1":"0";
	calculerS4();
	stage4.update();
});

const entreeB4 = new createjs.Shape();
stage4.addChild(entreeB4);
entreeB4.x = 25;
entreeB4.y = 250;
entreeB4.cursor = "pointer";
entreeB4.graphics.s("#000").ss(1).f("#FFF").dc(0, 0, 20);

const textB4 = new createjs.Text("B", "bold 20px Verdana", "#000");
stage4.addChild(textB4);
textB4.x = entreeB4.x;
textB4.y = entreeB4.y + 25;
textB4.textAlign = 'center';

const valeurB4 = new createjs.Text("0", "bold 20px Verdana", "#000");
stage4.addChild(valeurB4);
valeurB4.x = entreeB4.x;
valeurB4.y = entreeB4.y - 10;
valeurB4.textAlign = 'center';

entreeB4.addEventListener('click', function(){
	valeurB4.text = (valeurB4.text == "0")?"1":"0";
	calculerS4();
	stage4.update();
});

const sortie4 = new createjs.Shape();
stage4.addChild(sortie4);
sortie4.x = 375;
sortie4.y = entreeA4.y;
sortie4.graphics.s("#000").ss(1).f("#FFF").dc(0, 0, 20);

const textS4 = new createjs.Text("S", "bold 20px Verdana", "#000");
stage4.addChild(textS4);
textS4.x = sortie4.x;
textS4.y = sortie4.y - 40;
textS4.textAlign = 'center';

const valeurS4 = new createjs.Text("-", "bold 20px Verdana", "#000");
stage4.addChild(valeurS4);
valeurS4.x = sortie4.x;
valeurS4.y = sortie4.y - 10;
valeurS4.textAlign = 'center';

const retenue4 = new createjs.Shape();
stage4.addChild(retenue4);
retenue4.x = 375;
retenue4.y = entreeB4.y;
retenue4.graphics.s("#000").ss(1).f("#FFF").dc(0, 0, 20);

const textR4 = new createjs.Text("R", "bold 20px Verdana", "#000");
stage4.addChild(textR4);
textR4.x = retenue4.x;
textR4.y = retenue4.y + 25;
textR4.textAlign = 'center';

const valeurR4 = new createjs.Text("-", "bold 20px Verdana", "#000");
stage4.addChild(valeurR4);
valeurR4.x = retenue4.x;
valeurR4.y = retenue4.y - 10;
valeurR4.textAlign = 'center';

const network4 = new createjs.Shape();
stage4.addChild(network4);

network4.graphics.ss(5, "round").s('#31C2A9').mt(42, 134).lt(100, 100);
network4.graphics.ss(5, "round").s('#31C2A9').mt(42, 162).lt(100, 175);
network4.graphics.ss(5, "round").s('#5163ad').mt(46, 250).lt(100, 250);
network4.graphics.ss(5, "round").s('#5163ad').mt(42, 262).lt(100, 325);

network4.graphics.ss(5, "round").s('#31C2A9').mt(150, 100).lt(180, 125);
network4.graphics.ss(5, "round").s('#5163ad').mt(150, 325).lt(180, 149);
network4.graphics.ss(5, "round").s('#31C2A9').mt(150, 100).lt(180, 200);
network4.graphics.ss(5, "round").s('#ff00ff').mt(150, 175).lt(180, 275);
network4.graphics.ss(5, "round").s('#FF0000').mt(150, 250).lt(180, 224);
network4.graphics.ss(5, "round").s('#5163ad').mt(150, 325).lt(180, 299);

network4.graphics.ss(5, "round").s('#00ffff').mt(225, 212).lt(260, 238);
network4.graphics.ss(5, "round").s('#ffff00').mt(225, 287).lt(260, 262);

network4.graphics.ss(5, "round").s('#fba501').mt(225, 137).lt(352, 150);
network4.graphics.ss(5, "round").s('#00ff00').mt(300, 250).lt(352, 250);

const info4 = new createjs.Shape();
stage4.addChild(info4);
info4.x = 360;
info4.y = 350;
info4.graphics.s('#5163ad').ss(5).f("#1a3e70").rr(0, 0, 30, 40, 5, 5, 5, 5);
info4.cursor = 'pointer';

const textInfo4 = new createjs.Text("i", "bold 18px Verdana", "#fba501");
stage4.addChild(textInfo4);
textInfo4.x = info4.x + 12;
textInfo4.y = info4.y + 10;

const barreInfo4 = new createjs.Shape();
stage4.addChild(barreInfo4);
barreInfo4.x = 371;
barreInfo4.y = 375;
barreInfo4.graphics.s('#fba501').ss(3).mt(0, 0).lt(8, -8);

info4.addEventListener('mouseover', function(){
	info4.graphics.clear();
	info4.graphics.s('#fba501').ss(5).f("#5163ad").rr(0, 0, 30, 40, 5, 5, 5, 5);
	info4.cursor = 'pointer';
	barreInfo4.graphics.clear();
	barreInfo4.graphics.s('#1a3e70').ss(3).mt(0, 0).lt(8, -8);
	textInfo4.color = '#1a3e70';
	stage4.update();
});

info4.addEventListener('mouseout', function(){
	info4.graphics.clear();
	info4.graphics.s('#5163ad').ss(5).f("#1a3e70").rr(0, 0, 30, 40, 5, 5, 5, 5);
	info4.cursor = 'pointer';
	barreInfo4.graphics.clear();
	barreInfo4.graphics.s('#fba501').ss(3).mt(0, 0).lt(8, -8);
	textInfo4.color = '#fba501';
	stage4.update();
});
const containerLegende4 = new createjs.Container();
stage4.addChild(containerLegende4);

const textEntree4 = new createjs.Text("Entrée", "bold 18px Verdana", "#000");
containerLegende4.addChild(textEntree4);
textEntree4.x = 10;
textEntree4.y = 10;

const ligneEntree4 = new createjs.Shape();
containerLegende4.addChild(ligneEntree4);
ligneEntree4.graphics.s('#444').ss(3).sd([10, 10]).mt(90, 0).lt(90, 400);

const ligneSortie4 = new createjs.Shape();
containerLegende4.addChild(ligneSortie4);
ligneSortie4.graphics.s('#444').ss(3).sd([10, 10]).mt(320, 0).lt(320, 400);

const textNeurone4 = new createjs.Text("Réseau de\nneurones", "bold 18px Verdana", "#000");
containerLegende4.addChild(textNeurone4);
textNeurone4.x = 200;
textNeurone4.y = 10;
textNeurone4.textAlign = 'center';

const textSortie4 = new createjs.Text("Sortie", "bold 18px Verdana", "#000");
containerLegende4.addChild(textSortie4);
textSortie4.x = 330;
textSortie4.y = 10;

info4.addEventListener('click', function(){
	containerLegende4.visible = !containerLegende4.visible;
	barreInfo4.visible = containerLegende4.visible;
	stage4.update();
});

const corps4_1_1 = new createjs.Shape();
stage4.addChild(corps4_1_1);
corps4_1_1.x = 100;
corps4_1_1.y = 75;
corps4_1_1.graphics.s("#000").ss(1).f("#FFF").r(0, 0, 50, 50);

const text4_1_1 = new createjs.Text("ID", "bold 16px Verdana", "#000");
stage4.addChild(text4_1_1);
let metrics = text4_1_1.getMetrics();
text4_1_1.x = corps4_1_1.x + ((50 - metrics.width) / 2);
text4_1_1.y = corps4_1_1.y + ((50 - metrics.height) / 2);

const corps4_1_2 = new createjs.Shape();
stage4.addChild(corps4_1_2);
corps4_1_2.x = corps4_1_1.x;
corps4_1_2.y = corps4_1_1.y + 75;
corps4_1_2.graphics.s("#000").ss(1).f("#FFF").r(0, 0, 50, 50);
corps4_1_2.cursor = 'pointer';

const text4_1_2 = new createjs.Text("", "bold 16px Verdana", "#000");
stage4.addChild(text4_1_2);
metrics = text4_1_2.getMetrics();
text4_1_2.x = corps4_1_2.x + ((50 - metrics.width) / 2);
text4_1_2.y = corps4_1_2.y + ((50 - metrics.height) / 2);

corps4_1_2.addEventListener('click', function(evt){
	maj(corps4_1_2, text4_1_2);
	calculerS4();
	stage4.update();
});

const corps4_1_3 = new createjs.Shape();
stage4.addChild(corps4_1_3);
corps4_1_3.x = corps4_1_1.x;
corps4_1_3.y = corps4_1_2.y + 75;
corps4_1_3.graphics.s("#000").ss(1).f("#FFF").r(0, 0, 50, 50);
corps4_1_3.cursor = 'pointer';

const text4_1_3 = new createjs.Text("", "bold 16px Verdana", "#000");
stage4.addChild(text4_1_3);
metrics = text4_1_3.getMetrics();
text4_1_3.x = corps4_1_3.x + ((50 - metrics.width) / 2);
text4_1_3.y = corps4_1_3.y + ((50 - metrics.height) / 2);

corps4_1_3.addEventListener('click', function(evt){
	maj(corps4_1_3, text4_1_3);
	calculerS4();
	stage4.update();
});

const corps4_1_4 = new createjs.Shape();
stage4.addChild(corps4_1_4);
corps4_1_4.x = corps4_1_1.x;
corps4_1_4.y = corps4_1_3.y + 75;
corps4_1_4.graphics.s("#000").ss(1).f("#FFF").r(0, 0, 50, 50);

const text4_1_4 = new createjs.Text("ID", "bold 16px Verdana", "#000");
stage4.addChild(text4_1_4);
metrics = text4_1_4.getMetrics();
text4_1_4.x = corps4_1_4.x + ((50 - metrics.width) / 2);
text4_1_4.y = corps4_1_4.y + ((50 - metrics.height) / 2);

const corps4_2_1 = new createjs.Shape();
stage4.addChild(corps4_2_1);
corps4_2_1.x = 180;
corps4_2_1.y = 112;
corps4_2_1.graphics.s("#000").ss(1).f("#FFF").r(0, 0, 50, 50);
corps4_2_1.cursor = 'pointer';

function maj(corps, texte){
	switch (texte.text){
		case "ID":
			texte.text = "ET";
			break;
		case "ET":
			texte.text = "OU";
			break;
		case "OU":
			texte.text = "NON";
			break;
		case "NON":
			texte.text = "ID";
			break;
		default:
			texte.text = "ID";
			break;
	}
	metrics = texte.getMetrics();
	texte.x = corps.x + ((50 - metrics.width) / 2);
	texte.y = corps.y + ((50 - metrics.height) / 2);
}

corps4_2_1.addEventListener('click', function(evt){
	maj(corps4_2_1, text4_2_1);
	calculerS4();
	stage4.update();
});

const text4_2_1 = new createjs.Text("", "bold 16px Verdana", "#000");
stage4.addChild(text4_2_1);
metrics = text4_2_1.getMetrics();
text4_2_1.x = corps4_2_1.x + ((50 - metrics.width) / 2);
text4_2_1.y = corps4_2_1.y + ((50 - metrics.height) / 2);

const corps4_2_2 = new createjs.Shape();
stage4.addChild(corps4_2_2);
corps4_2_2.x = corps4_2_1.x;
corps4_2_2.y = corps4_2_1.y + 75;
corps4_2_2.graphics.s("#000").ss(1).f("#FFF").r(0, 0, 50, 50);
corps4_2_2.cursor = 'pointer';

const text4_2_2 = new createjs.Text("", "bold 16px Verdana", "#000");
stage4.addChild(text4_2_2);
metrics = text4_2_2.getMetrics();
text4_2_2.x = corps4_2_2.x + ((50 - metrics.width) / 2);
text4_2_2.y = corps4_2_2.y + ((50 - metrics.height) / 2);

corps4_2_2.addEventListener('click', function(evt){
	maj(corps4_2_2, text4_2_2);
	calculerS4();
	stage4.update();
});

const corps4_2_3 = new createjs.Shape();
stage4.addChild(corps4_2_3);
corps4_2_3.x = corps4_2_1.x;
corps4_2_3.y = corps4_2_2.y + 75;
corps4_2_3.graphics.s("#000").ss(1).f("#FFF").r(0, 0, 50, 50);
corps4_2_3.cursor = 'pointer';

const text4_2_3 = new createjs.Text("", "bold 16px Verdana", "#000");
stage4.addChild(text4_2_3);
metrics = text4_2_3.getMetrics();
text4_2_3.x = corps4_2_3.x + ((50 - metrics.width) / 2);
text4_2_3.y = corps4_2_3.y + ((50 - metrics.height) / 2);

corps4_2_3.addEventListener('click', function(evt){
	maj(corps4_2_3, text4_2_3);
	calculerS4();
	stage4.update();
});

const corps4_3_1 = new createjs.Shape();
stage4.addChild(corps4_3_1);
corps4_3_1.x = 260;
corps4_3_1.y = 225;
corps4_3_1.graphics.s("#000").ss(1).f("#FFF").r(0, 0, 50, 50);
corps4_3_1.cursor = 'pointer';

const text4_3_1 = new createjs.Text("", "bold 16px Verdana", "#000");
stage4.addChild(text4_3_1);
metrics = text4_3_1.getMetrics();
text4_3_1.x = corps4_3_1.x + ((50 - metrics.width) / 2);
text4_3_1.y = corps4_3_1.y + ((50 - metrics.height) / 2);

corps4_3_1.addEventListener('click', function(evt){
	maj(corps4_3_1, text4_3_1);
	calculerS4();
	stage4.update();
});

function calculerS4(){
 let A = parseInt(valeurA4.text);
 let B = parseInt(valeurB4.text);
 let S = portes[text4_2_1.text](portes[text4_1_1.text](A), portes[text4_1_4.text](B));
 valeurS4.text = S;

 let R = portes[text4_3_1.text](portes[text4_2_2.text](portes[text4_1_1.text](A), portes[text4_1_3.text](B)), portes[text4_2_3.text](portes[text4_1_2.text](A), portes[text4_1_4.text](B)));
 valeurR4.text = R;
}

verifier4.addEventListener('click', function(){
	const lignes = document.querySelectorAll("#add tbody tr");
	let corrects = 0;

	lignes.forEach((ligne, i) => {
		let a = parseInt(ligne.children[0].innerHTML);
		let b = parseInt(ligne.children[1].innerHTML);
		let s = portes[text4_2_1.text](portes[text4_1_1.text](a), portes[text4_1_4.text](b));
		let r = portes[text4_3_1.text](portes[text4_2_2.text](portes[text4_1_1.text](a), portes[text4_1_3.text](b)), portes[text4_2_3.text](portes[text4_1_2.text](a), portes[text4_1_4.text](b)));
		if (s == ligne.children[2].innerHTML && r == ligne.children[3].innerHTML){
			ligne.classList.add("ok");
			ligne.classList.remove("nok");
			corrects++;
		} else {
			ligne.classList.add("nok");
			ligne.classList.remove("ok");
		}
	});
});

stage4.update();
