const popup = document.querySelector("#popup");

const histoire = "<div class='block'><h2>Description</h2><p>L'objectif de cette section est de construire un réseau de neurones artificiels.</p></div>"

function preventDefaults (e) {
	e.preventDefault();
	e.stopPropagation();
}

document.querySelector("#close").addEventListener('click', function(){
    popup.style.display = "none";
});

document.querySelector("#history").addEventListener('click', function(){
    document.querySelector("#popup-content").innerHTML = histoire;
    popup.style.display = "flex";
});

document.querySelectorAll(".pop").forEach((item, i) => {
	item.addEventListener('click', function(){
		if (!this.classList.contains("disabled")){
			if (this.dataset["href"])
				location.href = this.dataset["href"];
			else if (this.dataset["exercice"]){
				document.querySelectorAll(".exo").forEach((item, i) => {
					item.classList.add("hide");
					if (i == this.dataset["exercice"])
						item.classList.remove("hide");
				});
				document.querySelector("#exercices .selected").classList.remove("selected");
				this.classList.add("selected");
			}
		}
	});
});
