// ==============================
//       CANVAS 1
// ==============================

let _wA3 = 0;
let _seuil3 = 0;

const canvas3 = document.getElementById("reseau3");
const stage3 = new createjs.Stage(canvas3);
stage3.enableMouseOver();
createjs.Touch.enable(stage3);
stage3.mouseMoveOutside = true;

const container3 = new createjs.Container();
stage3.addChild(container3);
container3.visible = false;

const dentriteA3 = new createjs.Shape();
stage3.addChild(dentriteA3);
dentriteA3.x = 70;
dentriteA3.y = 200;
dentriteA3.graphics.ss(8, "round").s('#31C2A9').mt(0, 0).bt(30, 10, 60, -10, 80, 0);

const axone3 = new createjs.Shape();
stage3.addChild(axone3);
axone3.x = 250;
axone3.y = 200;
axone3.graphics.ss(12, "round").s('#31C2A9').mt(0, 0).bt(30, 10, 60, -10, 80, 0);

const entreeA3 = new createjs.Shape();
stage3.addChild(entreeA3);
entreeA3.x = 50;
entreeA3.y = 200;
entreeA3.cursor = "pointer";
entreeA3.graphics.s("#000").ss(1).f("#FFF").dc(0, 0, 20);

const textA3 = new createjs.Text("A", "bold 20px Verdana", "#000");
stage3.addChild(textA3);
textA3.x = entreeA3.x;
textA3.y = entreeA3.y - 40;
textA3.textAlign = 'center';

const valeurA3 = new createjs.Text("0", "bold 20px Verdana", "#000");
stage3.addChild(valeurA3);
valeurA3.x = entreeA3.x;
valeurA3.y = entreeA3.y - 10;
valeurA3.textAlign = 'center';

entreeA3.addEventListener('click', function(){
	valeurA3.text = (valeurA3.text == "0")?"1":"0";
	calculerS3();
	stage3.update();
});

const sortie3 = new createjs.Shape();
stage3.addChild(sortie3);
sortie3.x = 350;
sortie3.y = 200;
sortie3.graphics.s("#000").ss(1).f("#FFF").dc(0, 0, 20);

const textS3 = new createjs.Text("S", "bold 20px Verdana", "#000");
stage3.addChild(textS3);
textS3.x = sortie3.x;
textS3.y = sortie3.y - 40;
textS3.textAlign = 'center';

const valeurS3 = new createjs.Text("-", "bold 20px Verdana", "#000");
stage3.addChild(valeurS3);
valeurS3.x = sortie3.x;
valeurS3.y = sortie3.y - 10;
valeurS3.textAlign = 'center';

const corpS3 = new createjs.Shape();
stage3.addChild(corpS3);
corpS3.x = 200 - 50;
corpS3.y = 200 - 37;
corpS3.graphics.s("#000").ss(1).f("#FFF").r(0, 0, 100, 75);

const seuilS3 = new createjs.Text("Seuil : 0", "bold 12px Verdana", "#000");
stage3.addChild(seuilS3);
seuilS3.x = corpS3.x + 60;
seuilS3.y = corpS3.y - 15;
seuilS3.textAlign = 'left';

const wA3 = new createjs.Text("w  : 0", "bold 12px Verdana", "#000");
stage3.addChild(wA3);
wA3.x = dentriteA3.x + 10;
wA3.y = dentriteA3.y - 20;
wA3.textAlign = 'left';

const w_A3 = new createjs.Text("A", "bold 12px Verdana", "#000");
stage3.addChild(w_A3);
w_A3.x = wA3.x + 10;
w_A3.y = wA3.y + 5;
w_A3.textAlign = 'left';

const xBarresA3 = wA3.x;
const yBarresA3 = wA3.y - 15;

const fondBarresA3 = new createjs.Shape();
container3.addChild(fondBarresA3);
fondBarresA3.graphics.s(1, 1, 1).ss('#000').f('#666').rr(xBarresA3, yBarresA3, 54, 8, 3);
fondBarresA3.graphics.s(3).ss('#444').mt(xBarresA3 + 4, yBarresA3 + 4).lt(xBarresA3 + 4 + 50, yBarresA3 + 4);

const curseurBarresA3 = new createjs.Shape();
container3.addChild(curseurBarresA3);
curseurBarresA3.cursor = 'pointer'
curseurBarresA3.graphics.s(1).ss('#000').f('#00F').dc(0, 0, 6).ef().es();
curseurBarresA3.x = xBarresA3 + ((_wA3 + 1) * 25);
curseurBarresA3.y = yBarresA3 + 4;

const levelBarresA3 = new createjs.Shape();
container3.addChild(levelBarresA3);

for (let niveau = 0; niveau <= 50; niveau += 25){
    const text = new createjs.Text((niveau - 25) / 25, "8px Verdana", "#000");
    text.x = xBarresA3 + niveau - 2;
    text.y = yBarresA3 - 20;
    text.cursor = 'pointer';
    text.textAlign = 'left';
    container3.addChild(text);

	levelBarresA3.graphics.s('#444').ss(1).mt(text.x + 3, text.y + 10).lt(text.x + 3, text.y + 15);

    text.addEventListener("click", function(evt){
		obj = evt.currentTarget;
		_wA3 = (obj.x - xBarresA3 + 2 - 25) / 25;
		curseurBarresA3.x = obj.x + 2;
		wA3.text = "w  : " + _wA3;
		calculerS3();
	    stage3.update();
	});
}


const combinaison3 = new createjs.Text("Combinaison : 0", "bold 10px Verdana", "#000");
stage3.addChild(combinaison3);
combinaison3.x = corpS3.x + 5;
combinaison3.y = corpS3.y + 25;
combinaison3.textAlign = 'left';

const formule3 = new createjs.Text("A * wA", "bold 10px Verdana", "#000");
stage3.addChild(formule3);
formule3.x = corpS3.x + 28;
formule3.y = corpS3.y + 45;
formule3.textAlign = 'left';

const xSeuil3 = seuilS3.x;
const ySeuil3 = seuilS3.y - 15;

const fondBarresS3 = new createjs.Shape();
container3.addChild(fondBarresS3);
fondBarresS3.graphics.s(1, 1, 1).ss('#000').f('#666').rr(xSeuil3, ySeuil3, 54, 8, 3);
fondBarresS3.graphics.s(3).ss('#444').mt(xSeuil3 + 4, ySeuil3 + 4).lt(xSeuil3 + 4 + 50, ySeuil3 + 4);

const curseurBarresS3 = new createjs.Shape();
container3.addChild(curseurBarresS3);
curseurBarresS3.cursor = 'pointer'
curseurBarresS3.graphics.s(1).ss('#000').f('#F00').dc(0, 0, 6).ef().es();
curseurBarresS3.x = xSeuil3 + ((_seuil3 + 1) * 25);
curseurBarresS3.y = ySeuil3 + 4;

const levelBarresS3 = new createjs.Shape();
container3.addChild(levelBarresS3);

for (let niveau = 0; niveau <= 50; niveau += 25){
    const text = new createjs.Text((niveau - 25) / 12.5, "8px Verdana", "#000");
    text.x = xSeuil3 + niveau - 2;
    text.y = ySeuil3 - 20;
    text.cursor = 'pointer';
    text.textAlign = 'left';
    container3.addChild(text);

	levelBarresS3.graphics.s('#444').ss(1).mt(text.x + 3, text.y + 10).lt(text.x + 3, text.y + 15);

    text.addEventListener("click", function(evt){
		obj = evt.currentTarget;
		_seuil3 = (((obj.x - xSeuil3) + 2 - 25) / 12.5);
		curseurBarresS3.x = obj.x + 2;
		seuilS3.text = "Seuil : " + _seuil3;
		calculerS3();
	    stage3.update();
	});
}

curseurBarresA3.addEventListener("mousedown", onMouseDownCurseur);
curseurBarresS3.addEventListener("mousedown", onMouseDownCurseur);

curseurBarresA3.addEventListener("pressmove", function(evt){
	obj = evt.currentTarget;
	obj.x = Math.min(Math.max(xBarresA3, evt.stageX + obj.offsetX), xBarresA3 + 50);
	_wA3 = (((obj.x - xBarresA3) - 25) / 25);
	wA3.text = "w  : " + _wA3;
	calculerS3();
	stage3.update();
});

curseurBarresS3.addEventListener("pressmove", function(evt){
	obj = evt.currentTarget;
	obj.x = Math.min(Math.max(xSeuil3, evt.stageX + obj.offsetX), xSeuil3 + 50);
	_seuil3 = (((obj.x - xSeuil3) - 25) / 12.5);
	seuilS3.text = "Seuil : " + _seuil3;
	calculerS3();
	stage3.update();
});

const info3 = new createjs.Shape();
stage3.addChild(info3);
info3.x = 320;
info3.y = 350;
info3.graphics.s('#5163ad').ss(5).f("#1a3e70").rr(0, 0, 30, 40, 5, 5, 5, 5);
info3.cursor = 'pointer';

const textInfo3 = new createjs.Text("i", "bold 18px Verdana", "#fba501");
stage3.addChild(textInfo3);
textInfo3.x = info3.x + 12;
textInfo3.y = info3.y + 10;

const barreInfo3 = new createjs.Shape();
stage3.addChild(barreInfo3);
barreInfo3.x = 331;
barreInfo3.y = 375;
barreInfo3.graphics.s('#fba501').ss(3).mt(0, 0).lt(8, -8);

info3.addEventListener('mouseover', function(){
	info3.graphics.clear();
	info3.graphics.s('#fba501').ss(5).f("#5163ad").rr(0, 0, 30, 40, 5, 5, 5, 5);
	info3.cursor = 'pointer';
	barreInfo3.graphics.clear();
	barreInfo3.graphics.s('#1a3e70').ss(3).mt(0, 0).lt(8, -8);
	textInfo3.color = '#1a3e70';
	stage3.update();
});

info3.addEventListener('mouseout', function(){
	info3.graphics.clear();
	info3.graphics.s('#5163ad').ss(5).f("#1a3e70").rr(0, 0, 30, 40, 5, 5, 5, 5);
	info3.cursor = 'pointer';
	barreInfo3.graphics.clear();
	barreInfo3.graphics.s('#fba501').ss(3).mt(0, 0).lt(8, -8);
	textInfo3.color = '#fba501';
	stage3.update();
});
const containerLegende3 = new createjs.Container();
stage3.addChild(containerLegende3);

const textEntree3 = new createjs.Text("Entrée", "bold 18px Verdana", "#000");
containerLegende3.addChild(textEntree3);
textEntree3.x = 10;
textEntree3.y = 10;

const ligneEntree3 = new createjs.Shape();
containerLegende3.addChild(ligneEntree3);
ligneEntree3.graphics.s('#444').ss(3).sd([10, 10]).mt(90, 0).lt(90, 400);

const ligneSortie3 = new createjs.Shape();
containerLegende3.addChild(ligneSortie3);
ligneSortie3.graphics.s('#444').ss(3).sd([10, 10]).mt(310, 0).lt(310, 400);

const textNeurone3 = new createjs.Text("Neurone", "bold 18px Verdana", "#000");
containerLegende3.addChild(textNeurone3);
textNeurone3.x = 160;
textNeurone3.y = 10;

const textSortie3 = new createjs.Text("Sortie", "bold 18px Verdana", "#000");
containerLegende3.addChild(textSortie3);
textSortie3.x = 330;
textSortie3.y = 10;

info3.addEventListener('click', function(){
	containerLegende3.visible = !containerLegende3.visible;
	barreInfo3.visible = containerLegende3.visible;
	stage3.update();
});

const show3 = new createjs.Shape();
stage3.addChild(show3);
show3.x = 360;
show3.y = 350;
show3.graphics.s('#5163ad').ss(5).f("#1a3e70").rr(0, 0, 30, 40, 5, 5, 5, 5);
show3.cursor = 'pointer';

show3.addEventListener('mouseover', function(){
	show3.graphics.clear();
	show3.graphics.s('#fba501').ss(5).f("#5163ad").rr(0, 0, 30, 40, 5, 5, 5, 5);
	show3.cursor = 'pointer';
	textShow3.color = '#1a3e70';
	stage3.update();
});

show3.addEventListener('mouseout', function(){
	show3.graphics.clear();
	show3.graphics.s('#5163ad').ss(5).f("#1a3e70").rr(0, 0, 30, 40, 5, 5, 5, 5);
	show3.cursor = 'pointer';
	textShow3.color = '#fba501';
	stage3.update();
});

show3.addEventListener('click', function(){
	container3.visible = !container3.visible;
	textShow3.text = (container3.visible)?"≠":"=";
	stage3.update();
});

const textShow3 = new createjs.Text("=", "bold 18px Verdana", "#fba501");
stage3.addChild(textShow3);
textShow3.x = show3.x + 7;
textShow3.y = show3.y + 10;

function calculerS3(){
	let combinaison = parseInt(valeurA3.text) * _wA3;
	combinaison3.text = "Combinaison : " + combinaison;
	if (combinaison > _seuil3){
		valeurS3.text = 1;
	} else {
		valeurS3.text = 0;
	}
}

verifier3.addEventListener('click', function(){
	const lignes = document.querySelectorAll("#non tbody tr");
	let corrects = 0;

	lignes.forEach((ligne, i) => {
		let c = ligne.children[0].innerHTML * _wA3;
		let s = (c > _seuil3)?1:0;
		if (s == ligne.children[1].innerHTML){
			ligne.classList.add("ok");
			ligne.classList.remove("nok");
			corrects++;
		} else {
			ligne.classList.add("nok");
			ligne.classList.remove("ok");
		}
	});

	if (corrects == 2){
		document.querySelector("#exercices").children[this.dataset["debloque"]].classList.remove("disabled");
	}
});

stage3.update();
