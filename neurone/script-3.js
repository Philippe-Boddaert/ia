const popup = document.querySelector("#popup");
const cHeureux = document.querySelector("#cHeureux");
const cTriste = document.querySelector("#cTriste");

const histoire = "<div class='block'><h2>Description</h2><p>L'objectif est de construire classifieur de smileys avec un seul neurone artificiel.</p><p>Étant donné un smiley en entrée, le classifieur indique si ce smiley représente un visage heureux ou triste.</p><p>Le classifieur nécessite que l'on attribue des pondérations aux pixels d’entrée de l’image en cliquant dessus.</p><p>Un clic définit la pondération sur 1 et un deuxième clic la définit sur -1.</p><p>L’activation positive indique que l’image est classée comme un visage heureux, ce qui peut être correct ou non, tandis que l’activation négative indique que l’image est classée comme visage triste.</p></div>"

function preventDefaults (e) {
	e.preventDefault();
	e.stopPropagation();
}

document.querySelector("#close").addEventListener('click', function(){
    popup.style.display = "none";
});

document.querySelector("#history").addEventListener('click', function(){
    document.querySelector("#popup-content").innerHTML = histoire;
    popup.style.display = "flex";
});

document.querySelectorAll(".pop").forEach((item, i) => {
	item.addEventListener('click', function(){
		if (this.dataset["href"])
			location.href = this.dataset["href"];
	});
});

document.querySelector("#tester").addEventListener('click', function(){
	if (!this.classList.contains("disabled")){
	    document.querySelector("#popup-content").innerHTML = "<div class='block'><h2>Test du classifieur</h2><p>Votre neurone aritificiel reconnait au moins 6 visages heureux et 6 visages tristes parmi les smileys de référence.</p><p>Est-il capable de classifier un smiley inconnu ?</p><p>&#10148; À Faire : Dessiner des visages heureux ou tristes et constater comment votre classifieur les classe.</p>";
	    let modele = document.querySelector("#image").cloneNode(true);
	    modele.id = "modele";
	    let div = document.createElement("div");
	    document.querySelector("#popup-content").appendChild(div);
	    div.appendChild(modele)
	    div.style.gap = "50px";
		div.style.justifyContent = "center";
	    let resultat = document.createElement("p");
	    resultat.style.textAlign = "center";
	    let table = document.createElement("table");
	    let caption = document.createElement("caption");
	    caption.innerHTML = "Smiley à classifier";
	    table.appendChild(caption);
	    table.classList.add("teste");

	    for (let i = 0; i < 5; i++) {
	      let tr = document.createElement("tr");
	      for (let j = 0; j < 5; j++) {
	        let td = document.createElement("td");
	        td.addEventListener('click', function(){
	          this.classList.toggle("activated");
	          switch (this.innerHTML) {
	            case "":
	              this.innerHTML = "1";
	              break;
	            case "1":
	              this.innerHTML = "";
	              break;
	          }
	          const modele = profile(document.querySelectorAll("#image td"));
	          const image = profile(table.querySelectorAll("td"));

	          const s = score(modele, image);
	          if (s < 0){
	            resultat.innerHTML = "Visage <span>triste</span> (Score : " + s + ")";
	          } else if (s > 0) {
	            resultat.innerHTML = "Visage <span>heureux</span> (Score : " + s + ")";
	          } else {
	            resultat.innerHTML = "Visage indéterminé (Score : " + s + ")";
	          }
	        })
	        tr.appendChild(td);
	      }
	      table.appendChild(tr);
	    }
	    div.appendChild(table);
	    let container = document.createElement("h3");
	    container.innerHTML = "Résultat de la classification";
	    document.querySelector("#popup-content").appendChild(container);
	    document.querySelector("#popup-content").appendChild(resultat);
	    popup.style.display = "flex";
	}
});


function profile(image){
  return Array.prototype.map.call(image, (pixel) => { return (pixel.innerHTML.length)?parseInt(pixel.innerHTML):0;});
}

function score(modele, image){
  let valeur = 0;

  for (let i = 0; i < modele.length; i++) {
    valeur += modele[i] * image[i];
  }
  return valeur;
}

function check(){
    const modele = profile(document.querySelectorAll("#image td"));
    let _heureux = 0;
    let _triste = 0;

    document.querySelectorAll(".heureux table").forEach((table, i) => {
      const image = profile(table.querySelectorAll("td"));

      if (score(modele, image) > 0){
        table.classList.add("full");
        _heureux++;
      } else {
        table.classList.remove("full");
      }
    });
    cHeureux.innerHTML = _heureux;

    document.querySelectorAll(".triste table").forEach((table, i) => {
      const image = profile(table.querySelectorAll("td"));

      if (score(modele, image) < 0){
        table.classList.add("full");
        _triste++;
      } else {
        table.classList.remove("full");
      }
    });
    cTriste.innerHTML = _triste;

	if (_heureux >= 6 && _triste >= 6){
		document.querySelector("#tester").classList.remove("disabled");
	} else {
		document.querySelector("#tester").classList.add("disabled");
	}
}

document.querySelectorAll("#image td").forEach((pixel, i) => {
  pixel.addEventListener('click', function(e){
    switch (this.innerHTML) {
      case "":
        this.innerHTML = "1";
        break;
      case "1":
        this.innerHTML = "-1";
        break;
      case "-1":
        this.innerHTML = "";
        break;
    }
    check();
  });
});
