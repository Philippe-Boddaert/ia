// ==============================
//       CANVAS 1
// ==============================

let _wA1 = 0;
let _wB1 = 0;
let _seuil1 = 0;

const canvas1 = document.getElementById("reseau1");
const stage1 = new createjs.Stage(canvas1);
stage1.enableMouseOver();
createjs.Touch.enable(stage1);
stage1.mouseMoveOutside = true;

const container1 = new createjs.Container();
stage1.addChild(container1);
container1.visible = false;

const dentriteA1 = new createjs.Shape();
stage1.addChild(dentriteA1);
dentriteA1.x = 70;
dentriteA1.y = 100;
dentriteA1.graphics.ss(8, "round").s('#31C2A9').mt(0, 0).bt(40, -20, 40, 95, 80, 75);

const dentriteB1 = new createjs.Shape();
stage1.addChild(dentriteB1);
dentriteB1.x = 70;
dentriteB1.y = 300;
dentriteB1.graphics.ss(8, "round").s('#31C2A9').mt(0, 0).bt(40, 20, 40, -95, 80, -75);

const axone1 = new createjs.Shape();
stage1.addChild(axone1);
axone1.x = 250;
axone1.y = 200;
axone1.graphics.ss(12, "round").s('#31C2A9').mt(0, 0).bt(30, 10, 60, -10, 80, 0);

const entreeA1 = new createjs.Shape();
stage1.addChild(entreeA1);
entreeA1.x = 50;
entreeA1.y = 100;
entreeA1.cursor = "pointer";
entreeA1.graphics.s("#000").ss(1).f("#FFF").dc(0, 0, 20);

const textA1 = new createjs.Text("A", "bold 20px Verdana", "#000");
stage1.addChild(textA1);
textA1.x = entreeA1.x;
textA1.y = entreeA1.y - 40;
textA1.textAlign = 'center';

const valeurA1 = new createjs.Text("0", "bold 20px Verdana", "#000");
stage1.addChild(valeurA1);
valeurA1.x = entreeA1.x;
valeurA1.y = entreeA1.y - 10;
valeurA1.textAlign = 'center';

entreeA1.addEventListener('click', function(){
	valeurA1.text = (valeurA1.text == "0")?"1":"0";
	calculerS1();
	stage1.update();
});

const entreeB1 = new createjs.Shape();
stage1.addChild(entreeB1);
entreeB1.x = 50;
entreeB1.y = 300;
entreeB1.cursor = "pointer";
entreeB1.graphics.s("#000").ss(1).f("#FFF").dc(0, 0, 20);

const valeurB1 = new createjs.Text("0", "bold 20px Verdana", "#000");
stage1.addChild(valeurB1);
valeurB1.x = entreeB1.x;
valeurB1.y = entreeB1.y - 10;
valeurB1.textAlign = 'center';

entreeB1.addEventListener('click', function(){
	valeurB1.text = (valeurB1.text == "0")?"1":"0";
	calculerS1();
	stage1.update();
});

const textB1 = new createjs.Text("B", "bold 20px Verdana", "#000");
stage1.addChild(textB1);
textB1.x = entreeB1.x;
textB1.y = entreeB1.y + 25;
textB1.textAlign = 'center';

const sortie1 = new createjs.Shape();
stage1.addChild(sortie1);
sortie1.x = 350;
sortie1.y = 200;
sortie1.graphics.s("#000").ss(1).f("#FFF").dc(0, 0, 20);

const textS1 = new createjs.Text("S", "bold 20px Verdana", "#000");
stage1.addChild(textS1);
textS1.x = sortie1.x;
textS1.y = sortie1.y - 40;
textS1.textAlign = 'center';

const valeurS1 = new createjs.Text("-", "bold 20px Verdana", "#000");
stage1.addChild(valeurS1);
valeurS1.x = sortie1.x;
valeurS1.y = sortie1.y - 10;
valeurS1.textAlign = 'center';

const corps1 = new createjs.Shape();
stage1.addChild(corps1);
corps1.x = 200 - 50;
corps1.y = 200 - 37;
corps1.graphics.s("#000").ss(1).f("#FFF").r(0, 0, 100, 75);

const seuilS1 = new createjs.Text("Seuil : 0", "bold 12px Verdana", "#000");
stage1.addChild(seuilS1);
seuilS1.x = corps1.x + 60;
seuilS1.y = corps1.y - 15;
seuilS1.textAlign = 'left';

const wA1 = new createjs.Text("w  : 0", "bold 12px Verdana", "#000");
stage1.addChild(wA1);
wA1.x = dentriteA1.x + 40;
wA1.y = dentriteA1.y + 5;
wA1.textAlign = 'left';

const w_A1 = new createjs.Text("A", "bold 12px Verdana", "#000");
stage1.addChild(w_A1);
w_A1.x = wA1.x + 10;
w_A1.y = wA1.y + 5;
w_A1.textAlign = 'left';

const xBarresA1 = wA1.x;
const yBarresA1 = wA1.y - 15;

const fondBarresA1 = new createjs.Shape();
container1.addChild(fondBarresA1);
fondBarresA1.graphics.s(1, 1, 1).ss('#000').f('#666').rr(xBarresA1, yBarresA1, 54, 8, 3);
fondBarresA1.graphics.s(3).ss('#444').mt(xBarresA1 + 4, yBarresA1 + 4).lt(xBarresA1 + 4 + 50, yBarresA1 + 4);

const curseurBarresA1 = new createjs.Shape();
container1.addChild(curseurBarresA1);
curseurBarresA1.cursor = 'pointer'
curseurBarresA1.graphics.s(1).ss('#000').f('#00F').dc(0, 0, 6).ef().es();
curseurBarresA1.x = xBarresA1 + ((_wA1 + 1) * 25);
curseurBarresA1.y = yBarresA1 + 4;

const levelBarresA1 = new createjs.Shape();
container1.addChild(levelBarresA1);

for (let niveau = 0; niveau <= 50; niveau += 25){
    const text = new createjs.Text((niveau - 25) / 25, "8px Verdana", "#000");
    text.x = xBarresA1 + niveau - 2;
    text.y = yBarresA1 - 20;
    text.cursor = 'pointer';
    text.textAlign = 'left';
    container1.addChild(text);

	levelBarresA1.graphics.s('#444').ss(1).mt(text.x + 3, text.y + 10).lt(text.x + 3, text.y + 15);

    text.addEventListener("click", function(evt){
		obj = evt.currentTarget;
		_wA1 = (obj.x - xBarresA1 + 2 - 25) / 25;
		curseurBarresA1.x = obj.x + 2;
		wA1.text = "w  : " + _wA1;
		calculerS1();
	    stage1.update();
	});
}

const wB1 = new createjs.Text("w  : 0", "bold 12px Verdana", "#000");
stage1.addChild(wB1);
wB1.x = dentriteB1.x + 40;
wB1.y = dentriteB1.y - 15;
wB1.textAlign = 'left';

const w_B1 = new createjs.Text("B", "bold 12px Verdana", "#000");
stage1.addChild(w_B1);
w_B1.x = wB1.x + 10;
w_B1.y = wB1.y + 5;
w_B1.textAlign = 'left';

const combinaison1 = new createjs.Text("Combinaison : 0", "bold 10px Verdana", "#000");
stage1.addChild(combinaison1);
combinaison1.x = corps1.x + 5;
combinaison1.y = corps1.y + 25;
combinaison1.textAlign = 'left';

const formule = new createjs.Text("A * wA + B * wB", "bold 10px Verdana", "#000");
stage1.addChild(formule);
formule.x = corps1.x + 3;
formule.y = corps1.y + 45;
formule.textAlign = 'left';

const xBarresB1 = wB1.x;
const yBarresB1 = wB1.y + 25;

const fondBarresB1 = new createjs.Shape();
container1.addChild(fondBarresB1);
fondBarresB1.graphics.s(1, 1, 1).ss('#000').f('#666').rr(xBarresB1, yBarresB1, 54, 8, 3);
fondBarresB1.graphics.s(3).ss('#444').mt(xBarresB1 + 4, yBarresB1 + 4).lt(xBarresB1 + 4 + 50, yBarresB1 + 4);

const curseurBarresB1 = new createjs.Shape();
container1.addChild(curseurBarresB1);
curseurBarresB1.cursor = 'pointer'
curseurBarresB1.graphics.s(1).ss('#000').f('#00F').dc(0, 0, 6).ef().es();
curseurBarresB1.x = xBarresB1 + ((_wB1 + 1) * 25);
curseurBarresB1.y = yBarresB1 + 4;

const levelBarresB1 = new createjs.Shape();
container1.addChild(levelBarresB1);

for (let niveau = 0; niveau <= 50; niveau += 25){
    const text = new createjs.Text((niveau - 25) / 25, "8px Verdana", "#000");
    text.x = xBarresB1 + niveau - 2;
    text.y = yBarresB1 + 20;
    text.cursor = 'pointer';
    text.textAlign = 'left';
    container1.addChild(text);

	levelBarresB1.graphics.s('#444').ss(1).mt(text.x + 3, text.y - 2).lt(text.x + 3, text.y - 7);

    text.addEventListener("click", function(evt){
		obj = evt.currentTarget;
		_wB1 = (obj.x - xBarresB1 + 2 - 25) / 25;
		curseurBarresB1.x = obj.x + 2;
		wB1.text = "w  : " + _wB1;
		calculerS1();
	    stage1.update();
	});
}

const xSeuil1 = seuilS1.x;
const ySeuil1 = seuilS1.y - 15;

const fondBarresS1 = new createjs.Shape();
container1.addChild(fondBarresS1);
fondBarresS1.graphics.s(1, 1, 1).ss('#000').f('#666').rr(xSeuil1, ySeuil1, 54, 8, 3);
fondBarresS1.graphics.s(3).ss('#444').mt(xSeuil1 + 4, ySeuil1 + 4).lt(xSeuil1 + 4 + 50, ySeuil1 + 4);

const curseurBarresS1 = new createjs.Shape();
container1.addChild(curseurBarresS1);
curseurBarresS1.cursor = 'pointer';
curseurBarresS1.graphics.s(1).ss('#000').f('#F00').dc(0, 0, 6).ef().es();
curseurBarresS1.x = xSeuil1 + ((_seuil1 + 1) * 25);
curseurBarresS1.y = ySeuil1 + 4;

const levelBarresS1 = new createjs.Shape();
container1.addChild(levelBarresS1);

for (let niveau = 0; niveau <= 50; niveau += 25){
    const text = new createjs.Text((niveau - 25) / 12.5, "8px Verdana", "#000");
    text.x = xSeuil1 + niveau - 2;
    text.y = ySeuil1 - 20;
    text.cursor = 'pointer';
    text.textAlign = 'left';
    container1.addChild(text);

	levelBarresS1.graphics.s('#444').ss(1).mt(text.x + 3, text.y + 10).lt(text.x + 3, text.y + 15);

    text.addEventListener("click", function(evt){
		obj = evt.currentTarget;
		_seuil1 = (((obj.x - xSeuil1) + 2 - 25) / 12.5);
		curseurBarresS1.x = obj.x + 2;
		seuilS1.text = "Seuil : " + _seuil1;
		calculerS1();
	    stage1.update();
	});
}

curseurBarresA1.addEventListener("mousedown", onMouseDownCurseur);
curseurBarresB1.addEventListener("mousedown", onMouseDownCurseur);
curseurBarresS1.addEventListener("mousedown", onMouseDownCurseur);

curseurBarresA1.addEventListener("pressmove", function(evt){
	obj = evt.currentTarget;
	obj.x = Math.min(Math.max(xBarresA1, evt.stageX + obj.offsetX), xBarresA1 + 50);
	_wA1 = (((obj.x - xBarresA1) - 25) / 25);
	wA1.text = "w  : " + _wA1;
	calculerS1();
	stage1.update();
});

curseurBarresB1.addEventListener("pressmove", function(evt){
	obj = evt.currentTarget;
	obj.x = Math.min(Math.max(xBarresB1, evt.stageX + obj.offsetX), xBarresB1 + 50);
	_wB1 = (((obj.x - xBarresB1) - 25) / 25);
	wB1.text = "w  : " + _wB1;
	calculerS1();
	stage1.update();
});

curseurBarresS1.addEventListener("pressmove", function(evt){
	obj = evt.currentTarget;
	obj.x = Math.min(Math.max(xSeuil1, evt.stageX + obj.offsetX), xSeuil1 + 50);
	_seuil1 = (((obj.x - xSeuil1) - 25) / 12.5);
	seuilS1.text = "Seuil : " + _seuil1;
	calculerS1();
	stage1.update();
});

const info1 = new createjs.Shape();
stage1.addChild(info1);
info1.x = 320;
info1.y = 350;
info1.graphics.s('#5163ad').ss(5).f("#1a3e70").rr(0, 0, 30, 40, 5, 5, 5, 5);
info1.cursor = 'pointer';

const textInfo1 = new createjs.Text("i", "bold 18px Verdana", "#fba501");
stage1.addChild(textInfo1);
textInfo1.x = info1.x + 12;
textInfo1.y = info1.y + 10;

const barreInfo1 = new createjs.Shape();
stage1.addChild(barreInfo1);
barreInfo1.x = 331;
barreInfo1.y = 375;
barreInfo1.graphics.s('#fba501').ss(3).mt(0, 0).lt(8, -8);

info1.addEventListener('mouseover', function(){
	info1.graphics.clear();
	info1.graphics.s('#fba501').ss(5).f("#5163ad").rr(0, 0, 30, 40, 5, 5, 5, 5);
	info1.cursor = 'pointer';
	barreInfo1.graphics.clear();
	barreInfo1.graphics.s('#1a3e70').ss(3).mt(0, 0).lt(8, -8);
	textInfo1.color = '#1a3e70';
	stage1.update();
});

info1.addEventListener('mouseout', function(){
	info1.graphics.clear();
	info1.graphics.s('#5163ad').ss(5).f("#1a3e70").rr(0, 0, 30, 40, 5, 5, 5, 5);
	info1.cursor = 'pointer';
	barreInfo1.graphics.clear();
	barreInfo1.graphics.s('#fba501').ss(3).mt(0, 0).lt(8, -8);
	textInfo1.color = '#fba501';
	stage1.update();
});
const containerLegende1 = new createjs.Container();
stage1.addChild(containerLegende1);

const textEntree1 = new createjs.Text("Entrée", "bold 18px Verdana", "#000");
containerLegende1.addChild(textEntree1);
textEntree1.x = 10;
textEntree1.y = 10;

const ligneEntree1 = new createjs.Shape();
containerLegende1.addChild(ligneEntree1);
ligneEntree1.graphics.s('#444').ss(3).sd([10, 10]).mt(90, 0).lt(90, 400);

const ligneSortie1 = new createjs.Shape();
containerLegende1.addChild(ligneSortie1);
ligneSortie1.graphics.s('#444').ss(3).sd([10, 10]).mt(310, 0).lt(310, 400);

const textNeurone1 = new createjs.Text("Neurone", "bold 18px Verdana", "#000");
containerLegende1.addChild(textNeurone1);
textNeurone1.x = 160;
textNeurone1.y = 10;

const textSortie1 = new createjs.Text("Sortie", "bold 18px Verdana", "#000");
containerLegende1.addChild(textSortie1);
textSortie1.x = 330;
textSortie1.y = 10;

info1.addEventListener('click', function(){
	containerLegende1.visible = !containerLegende1.visible;
	barreInfo1.visible = containerLegende1.visible;
	stage1.update();
});

const show1 = new createjs.Shape();
stage1.addChild(show1);
show1.x = 360;
show1.y = 350;
show1.graphics.s('#5163ad').ss(5).f("#1a3e70").rr(0, 0, 30, 40, 5, 5, 5, 5);
show1.cursor = 'pointer';

show1.addEventListener('mouseover', function(){
	show1.graphics.clear();
	show1.graphics.s('#fba501').ss(5).f("#5163ad").rr(0, 0, 30, 40, 5, 5, 5, 5);
	show1.cursor = 'pointer';
	textShow1.color = '#1a3e70';
	stage1.update();
});

show1.addEventListener('mouseout', function(){
	show1.graphics.clear();
	show1.graphics.s('#5163ad').ss(5).f("#1a3e70").rr(0, 0, 30, 40, 5, 5, 5, 5);
	show1.cursor = 'pointer';
	textShow1.color = '#fba501';
	stage1.update();
});

show1.addEventListener('click', function(){
	container1.visible = !container1.visible;
	textShow1.text = (container1.visible)?"≠":"=";
	stage1.update();
});

const textShow1 = new createjs.Text("=", "bold 18px Verdana", "#fba501");
stage1.addChild(textShow1);
textShow1.x = show1.x + 7;
textShow1.y = show1.y + 10;

function calculerS1(){
	let combinaison = parseInt(valeurA1.text) * _wA1 + parseInt(valeurB1.text) * _wB1;
	combinaison1.text = "Combinaison : " + combinaison;
	if (combinaison > _seuil1){
		valeurS1.text = 1;
	} else {
		valeurS1.text = 0;
	}
}

verifier1.addEventListener('click', function(){
	const lignes = document.querySelectorAll("#et tbody tr");
	let corrects = 0;

	lignes.forEach((ligne, i) => {
		let c = (ligne.children[0].innerHTML * _wA1) + (ligne.children[1].innerHTML * _wB1);
		let s = (c > _seuil1)?1:0;
		if (s == ligne.children[2].innerHTML){
			ligne.classList.add("ok");
			ligne.classList.remove("nok");
			corrects++;
		} else {
			ligne.classList.add("nok");
			ligne.classList.remove("ok");
		}
	});

	if (corrects == 4){
		document.querySelector("#exercices").children[this.dataset["debloque"]].classList.remove("disabled");
	}
});

stage1.update();
