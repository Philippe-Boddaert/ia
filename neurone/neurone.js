// ===== données =====

// ==============================
//       CANVAS 1
// ==============================

const canvas1 = document.getElementById("schema");
const stage1 = new createjs.Stage(canvas1);
stage1.enableMouseOver();
createjs.Touch.enable(stage1);
stage1.mouseMoveOutside = true;

// ===== VARIABLE A =====
const dentriteNO = new createjs.Shape();
stage1.addChild(dentriteNO);
dentriteNO.x = 110;
dentriteNO.y = 98;
dentriteNO.graphics.ss(8, "round").s('#31C2A9').mt(0, 0).bt(180, 0, 150, -50, 250, 100);

const axoneNO = new createjs.Shape();
stage1.addChild(axoneNO);
axoneNO.x = 0;
axoneNO.y = 50;
axoneNO.graphics.ss(20, "round").s('#4744A2').mt(0, 0).qt(25, 50, 100, 50);
axoneNO.graphics.s('#FFF').de(100, 45, 20, 10);

const dentriteO = new createjs.Shape();
stage1.addChild(dentriteO);
dentriteO.x = 120;
dentriteO.y = 330;
dentriteO.graphics.ss(8, "round").s('#31C2A9').mt(0, 0).bt(50, 75, 100, -75, 250, -75);

const axoneSO = new createjs.Shape();
stage1.addChild(axoneSO);
axoneSO.x = 0;
axoneSO.y = 350;
axoneSO.graphics.ss(20, "round").s('#4744A2').mt(0, 0).qt(25, -100, 100, -50);
axoneSO.graphics.s('#FFF').de(100, -50, 20, 10);
axoneSO.rotation = 15;

const dentriteSO = new createjs.Shape();
stage1.addChild(dentriteSO);
dentriteSO.x = 0;
dentriteSO.y = 450;
dentriteSO.graphics.ss(8, "round").s('#31C2A9').mt(0, 0).bt(180, -150, 200, 200, 250, -100);
dentriteSO.graphics.mt(250, -100).qt(266, -150, 350, -125);

const corpsH = new createjs.Shape();
stage1.addChild(corpsH);
corpsH.x = 350;
corpsH.y = 210;
corpsH.graphics.ss(1, "round").s('#31C2A9').f("#31C2A9").mt(0, 0).bt(0, -50, 100, 50, 250, 40).lt(270, 70).bt(100, 150, -100, 200, 0, 100).bt(50, -10, -10, 50, 0, 0).cp();

const boutonA = new createjs.Shape();
stage1.addChild(boutonA);
boutonA.x = 200;
boutonA.y = 150;
boutonA.cursor = "pointer";
boutonA.graphics.ss(5).s("#FFF").mt(0, 0).lt(45, -60);
boutonA.graphics.f("#FFF").dc(0, 0, 20);


const textA = new createjs.Text("A", "bold 20px Verdana", "#000");
stage1.addChild(textA);
textA.x = boutonA.x;
textA.y = boutonA.y - 10;
textA.textAlign = 'center';

boutonA.addEventListener('click', function(evt){
  document.querySelector("#popup-content").innerHTML = "<h1>Dendrite</h1><p>Un dendrite représente l'entrée d'un neurone.</p><p>Un dendrite est un fils qui alimente le corps cellulaire en influx électrique.</p><p>Ainsi, il permet au neurone de recevoir des informations.</p>";
  popup.style.display = "flex";
});

const boutonB = new createjs.Shape();
stage1.addChild(boutonB);
boutonB.x = 475;
boutonB.y = 150;
boutonB.cursor = "pointer";
boutonB.graphics.ss(5).s("#FFF").mt(0, 0).lt(-45, 80);
boutonB.graphics.f("#FFF").dc(0, 0, 20);

const textB = new createjs.Text("B", "bold 20px Verdana", "#000");
stage1.addChild(textB);
textB.x = boutonB.x;
textB.y = boutonB.y - 10;
textB.textAlign = 'center';

boutonB.addEventListener('click', function(evt){
  document.querySelector("#popup-content").innerHTML = "<h1>Corps cellulaire</h1><p>Le corps cellulaire représente la partie centrale d'un neurone.</p><p>Le corps cellulaire traite l'information provenant des dendrites et renvoie le résultat de ce traitement aux autres neurones auxquels il est connecté via les axones.</p>";
  popup.style.display = "flex";
});

const axoneE = new createjs.Shape();
stage1.addChild(axoneE);
axoneE.x = 600;
axoneE.y = 260;
axoneE.graphics.ss(20, "round").s('#31C2A9').mt(0, 0).bt(25, 50, 150, -50, 200, -30);

const boutonC = new createjs.Shape();
stage1.addChild(boutonC);
boutonC.x = 660;
boutonC.y = 200;
boutonC.cursor = "pointer";
boutonC.graphics.ss(5).s("#FFF").mt(0, 0).lt(-25, 70);
boutonC.graphics.f("#FFF").dc(0, 0, 20);

const textC = new createjs.Text("C", "bold 20px Verdana", "#000");
stage1.addChild(textC);
textC.x = boutonC.x;
textC.y = boutonC.y - 10;
textC.textAlign = 'center';

boutonC.addEventListener('click', function(evt){
  document.querySelector("#popup-content").innerHTML = "<h1>Axone</h1><p>Un axone représente la sortie d'un neurone.</p><p>Un axone est un fils qui reçoit le traitement du corps cellulaire sous la forme d'un influx électrique.</p><p>Ainsi, il permet au neurone d'envoyer des informations au reste du réseau de neurones.</p>";
  popup.style.display = "flex";
});

const boutonD = new createjs.Shape();
stage1.addChild(boutonD);
boutonD.x = 775;
boutonD.y = 180;
boutonD.cursor = "pointer";
boutonD.graphics.ss(5).s("#FFF").mt(0, 0).lt(45, 45);
boutonD.graphics.f("#FFF").dc(0, 0, 20);

const textD = new createjs.Text("D", "bold 20px Verdana", "#000");
stage1.addChild(textD);
textD.x = boutonD.x;
textD.y = boutonD.y - 10;
textD.textAlign = 'center';

boutonD.addEventListener('click', function(evt){
  document.querySelector("#popup-content").innerHTML = "<h1>Synapse</h1><p>Une synapse est une zone de contact fonctionnelle qui s'établit entre deux neurones.</p><p>Une synapse assure l'interconnexion dans le réseau de neurones.</p><p>Ainsi, elle permet d'acheminer l'influx électrique d'un axone présynaptique à une dendrite postsynaptique.</p>";
  popup.style.display = "flex";
});

const dentriteE = new createjs.Shape();
stage1.addChild(dentriteE);
dentriteE.x = 820;
dentriteE.y = 225;
dentriteE.graphics.ss(8, "round").s('#4744A2').mt(0, 0).bt(10, 0, 15, -50, 900, -100);

const synapse = new createjs.Shape();
stage1.addChild(synapse);
synapse.x = 700;
synapse.y = 220;
synapse.graphics.ss(20, "round").s('#FFF').f("#FFF").de(100, 45, 20, 10);
synapse.rotation = -20;

stage1.update();
