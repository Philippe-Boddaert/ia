document.querySelectorAll(".info").forEach((item, i) => {
	item.addEventListener('click', function(){
		document.querySelector("#popup-content").innerHTML = this.dataset["info"];
	    popup.style.display = "flex";
	});
});

document.querySelector("#verifier").addEventListener('click', function(){
	let images = document.querySelector("#questionnaire tbody tr").children;
	let questions = document.querySelectorAll("#questionnaire tbody select");
	let corrects = 0;

	questions.forEach((item, i) => {
		if (item.value == item.dataset["reponse"]){
			images[i].classList.remove("nok");
			images[i].classList.add("ok");
			corrects++;
		} else {
			images[i].classList.remove("ok");
			images[i].classList.add("nok");
		}
	});

	if (corrects == images.length){
		document.querySelector("#exercices").children[this.dataset["debloque"]].classList.remove("disabled");
	}
});
